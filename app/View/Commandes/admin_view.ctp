<div class="commandes view">
<h2><?php  echo __('Commande'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($commande['Commande']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($commande['Commande']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client'); ?></dt>
		<dd>
			<?php echo $this->Html->link($commande['Client']['id'], array('controller' => 'clients', 'action' => 'view', $commande['Client']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Commande'), array('action' => 'edit', $commande['Commande']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Commande'), array('action' => 'delete', $commande['Commande']['id']), null, __('Are you sure you want to delete # %s?', $commande['Commande']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('controller' => 'livraisons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Livraisons'); ?></h3>
	<?php if (!empty($commande['Livraison'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Facture Id'); ?></th>
		<th><?php echo __('Commande Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($commande['Livraison'] as $livraison): ?>
		<tr>
			<td><?php echo $livraison['id']; ?></td>
			<td><?php echo $livraison['facture_id']; ?></td>
			<td><?php echo $livraison['commande_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'livraisons', 'action' => 'view', $livraison['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'livraisons', 'action' => 'edit', $livraison['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'livraisons', 'action' => 'delete', $livraison['id']), null, __('Are you sure you want to delete # %s?', $livraison['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
