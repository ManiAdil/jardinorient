<div class="typesoutils form">
<?php echo $this->Form->create('Typesoutil'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Typesoutil'); ?></legend><br/><br/>
	<?php
		echo $this->Form->input('id:<br/>');
		echo $this->Form->input('outils_agricoles:<br/>');
		echo $this->Form->input('accessoires:<br/>');
	?>
	</fieldset>
	<br/><br/>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
<br/>
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Typesoutil.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Typesoutil.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Outils'), array('controller' => 'outils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outil'), array('controller' => 'outils', 'action' => 'add')); ?> </li>
	</ul>
</div>
