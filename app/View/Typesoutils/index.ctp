<div class="typesoutils index">
	<h2><?php echo __('Typesoutils'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('outils_agricoles'); ?></th>
			<th><?php echo $this->Paginator->sort('accessoires'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($typesoutils as $typesoutil): ?>
	<tr>
		<td><?php echo h($typesoutil['Typesoutil']['id']); ?>&nbsp;</td>
		<td><?php echo h($typesoutil['Typesoutil']['outils_agricoles']); ?>&nbsp;</td>
		<td><?php echo h($typesoutil['Typesoutil']['accessoires']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $typesoutil['Typesoutil']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $typesoutil['Typesoutil']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $typesoutil['Typesoutil']['id']), null, __('Are you sure you want to delete # %s?', $typesoutil['Typesoutil']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Outils'), array('controller' => 'outils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outil'), array('controller' => 'outils', 'action' => 'add')); ?> </li>
	</ul>
</div>
