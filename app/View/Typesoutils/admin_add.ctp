<div class="typesoutils form">
<?php echo $this->Form->create('Typesoutil'); ?>
	<fieldset>
		<legend div align="center"><font face="Script mt bold" size="7" color="#2ED135"><?php echo __('Admin Add Typesoutil'); ?><br/></font></legend>
	<?php
		echo $this->Form->input('outils_agricoles:<br/>');
 
		echo $this->Form->input('accessoires:<br/>');
        
	?>
	</fieldset><br/>
	:<br/>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<br/>
<br/>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Typesoutils'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Outils'), array('controller' => 'outils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outil'), array('controller' => 'outils', 'action' => 'add')); ?> </li>
	</ul>
</div>
