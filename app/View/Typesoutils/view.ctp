<div class="typesoutils view">
<h2><?php  echo __('Typesoutil'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($typesoutil['Typesoutil']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Outils Agricoles'); ?></dt>
		<dd>
			<?php echo h($typesoutil['Typesoutil']['outils_agricoles']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Accessoires'); ?></dt>
		<dd>
			<?php echo h($typesoutil['Typesoutil']['accessoires']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Typesoutil'), array('action' => 'edit', $typesoutil['Typesoutil']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Typesoutil'), array('action' => 'delete', $typesoutil['Typesoutil']['id']), null, __('Are you sure you want to delete # %s?', $typesoutil['Typesoutil']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Outils'), array('controller' => 'outils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outil'), array('controller' => 'outils', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Outils'); ?></h3>
	<?php if (!empty($typesoutil['Outil'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id O'); ?></th>
		<th><?php echo __('Nom O'); ?></th>
		<th><?php echo __('Definition O'); ?></th>
		<th><?php echo __('Photo O'); ?></th>
		<th><?php echo __('Prix O'); ?></th>
		<th><?php echo __('Quantité O'); ?></th>
		<th><?php echo __('Typesoutil Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($typesoutil['Outil'] as $outil): ?>
		<tr>
			<td><?php echo $outil['id_o']; ?></td>
			<td><?php echo $outil['nom_o']; ?></td>
			<td><?php echo $outil['definition_o']; ?></td>
			<td><?php echo $outil['photo_o']; ?></td>
			<td><?php echo $outil['prix_o']; ?></td>
			<td><?php echo $outil['quantité_o']; ?></td>
			<td><?php echo $outil['typesoutil_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'outils', 'action' => 'view', $outil['id_o'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'outils', 'action' => 'edit', $outil['id_o'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'outils', 'action' => 'delete', $outil['id_o']), null, __('Are you sure you want to delete # %s?', $outil['id_o'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Outil'), array('controller' => 'outils', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
