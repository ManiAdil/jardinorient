<!-- Start of blog right light -->
<div class="blog_right_light">
<h3>Mot de directeur</h3>

<?php echo $this->element('/mot_de_directeur'); ?> 

<h3>Nouveautés</h3>

<?php echo $this->element('/news'); ?> 

<h3>Sites Amis</h3>

<?php echo $this->element('/sitesamis'); ?> 

<h3>Nos Coordonées</h3>

<?php echo $this->element('/coordonees'); ?> 

</div><!-- End of blog right light -->