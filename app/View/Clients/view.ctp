<div class="clients view">
<p class="inscriptionf"><?php  echo __('Client'); ?> :</p>
	<dl>
		<u><b><dt><?php echo __('Id'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($client['Client']['id']); ?><br>
			&nbsp;
		</dd>
		<u><b><dt><?php echo __('Nom'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($client['Client']['nom']); ?><br>
			&nbsp;
		</dd>
		<u><b><dt><?php echo __('Prenom'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($client['Client']['prenom']); ?><br>
			&nbsp;
		</dd>
		<u><b><dt><?php echo __('Adresse'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($client['Client']['adresse']); ?><br>
			&nbsp;
		</dd>
		<u><b><dt><?php echo __('Tel'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($client['Client']['tel']); ?><br>
			&nbsp;
		</dd>
		<u><b><dt><?php echo __('Ville'); ?> :</dt></b></u><br>
		<dd>
			<?php echo $this->Html->link($client['Ville']['id'], array('controller' => 'villes', 'action' => 'view', $client['Ville']['id'])); ?><br>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Client'), array('action' => 'edit', $client['Client']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Client'), array('action' => 'delete', $client['Client']['id']), null, __('Are you sure you want to delete # %s?', $client['Client']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('controller' => 'villes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville'), array('controller' => 'villes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Commandes'); ?></h3>
	<?php if (!empty($client['commande'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Client Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($client['commande'] as $commande): ?>
		<tr>
			<td><?php echo $commande['id']; ?></td>
			<td><?php echo $commande['date']; ?></td>
			<td><?php echo $commande['client_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'commandes', 'action' => 'view', $commande['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'commandes', 'action' => 'edit', $commande['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'commandes', 'action' => 'delete', $commande['id']), null, __('Are you sure you want to delete # %s?', $commande['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
