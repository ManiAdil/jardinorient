<div>
	<h3>Liste des clients </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('nom'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('prenom'); ?></th>	
			<th class="data"><?php echo $this->Paginator->sort('adresse'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('tel'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('ville_id'); ?></th>
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($clients as $client):?>
	<tr>
		<td class="data"><?php echo h($client['Client']['id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($client['Client']['nom']); ?>&nbsp;</td>
		<td class="data"><?php echo h($client['Client']['prenom']); ?>&nbsp;</td>
		<td class="data"><?php echo h($client['Client']['adresse']); ?>&nbsp;</td>
		<td class="data"><?php echo h($client['Client']['tel']); ?>&nbsp;</td>
         <td class="data"><?php echo h($client['Client']['ville_id']); ?>&nbsp;</td>
             <td  class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $client['Client']['id'])); ?>
			<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $client['Client']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $client['Client']['id']), null, __('Are you sure you want to delete # %s?', $client['Client']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
