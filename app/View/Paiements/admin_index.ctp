<div>
	<h3>Liste des paiements </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('date'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('livraison_id'); ?></th>	
			
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($paiements as $paiement):?>
	<tr>
		<td class="data"><?php echo h($paiement['Paiement']['id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($paiement['Paiement']['date']); ?>&nbsp;</td>
		<td class="data"><?php echo h($paiement['Paiement']['livraison_id']); ?>&nbsp;</td>
		
             <td  class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $paiement['Paiement']['id'])); ?>
			<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $paiement['Paiement']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $paiement['Paiement']['id']), null, __('Are you sure you want to delete # %s?', $paiement['Paiement']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
