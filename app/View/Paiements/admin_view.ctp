<div class="paiements view">
<h2><?php  echo __('Paiement'); ?></h2>
	<dl>
		<dt><?php echo __('Livraison'); ?></dt>
		<dd>
			<?php echo $this->Html->link($paiement['Livraison']['id'], array('controller' => 'livraisons', 'action' => 'view', $paiement['Livraison']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($paiement['Paiement']['date']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Livraison Id'); ?></dt>
		<dd>
			<?php echo h($paiement['Paiement']['livraison_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Paiement'), array('action' => 'edit', $paiement['Paiement']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Paiement'), array('action' => 'delete', $paiement['Paiement']['id']), null, __('Are you sure you want to delete # %s?', $paiement['Paiement']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Paiements'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paiement'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('controller' => 'livraisons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
	</ul>
</div>
