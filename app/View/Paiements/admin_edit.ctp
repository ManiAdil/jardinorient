<div class="paiements form">
<?php echo $this->Form->create('Paiement'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Paiement'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('date');
		echo $this->Form->input('livraison_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Paiement.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Paiement.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Paiements'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('controller' => 'livraisons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
	</ul>
</div>
