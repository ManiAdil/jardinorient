<div class="contenugs form">
<?php echo $this->Form->create('Contenug'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Contenug'); ?></legend>
	<?php
		echo $this->Form->input('commande_id');
		echo $this->Form->input('grain_id');
		echo $this->Form->input('quantite');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Contenug.commande_id grain_id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Contenug.commande_id grain_id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Contenugs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Grains'), array('controller' => 'grains', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grain'), array('controller' => 'grains', 'action' => 'add')); ?> </li>
	</ul>
</div>
