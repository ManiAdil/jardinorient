<div class="contenugs view">
<h2><?php  echo __('Contenug'); ?></h2>
	<dl>
		<dt><?php echo __('Commande'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contenug['Commande']['id'], array('controller' => 'commandes', 'action' => 'view', $contenug['Commande']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Grain'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contenug['Grain']['id'], array('controller' => 'grains', 'action' => 'view', $contenug['Grain']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantite'); ?></dt>
		<dd>
			<?php echo h($contenug['Contenug']['quantite']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contenug'), array('action' => 'edit', $contenug['Contenug']['commande_id grain_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contenug'), array('action' => 'delete', $contenug['Contenug']['commande_id grain_id']), null, __('Are you sure you want to delete # %s?', $contenug['Contenug']['commande_id grain_id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contenugs'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contenug'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Grains'), array('controller' => 'grains', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grain'), array('controller' => 'grains', 'action' => 'add')); ?> </li>
	</ul>
</div>
