<div class="contenugs index">
	<h2><?php echo __('Contenugs'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('commande_id'); ?></th>
			<th><?php echo $this->Paginator->sort('grain_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantite'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contenugs as $contenug): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($contenug['Commande']['id'], array('controller' => 'commandes', 'action' => 'view', $contenug['Commande']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($contenug['Grain']['id'], array('controller' => 'grains', 'action' => 'view', $contenug['Grain']['id'])); ?>
		</td>
		<td><?php echo h($contenug['Contenug']['quantite']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contenug['Contenug']['commande_id grain_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contenug['Contenug']['commande_id grain_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contenug['Contenug']['commande_id grain_id']), null, __('Are you sure you want to delete # %s?', $contenug['Contenug']['commande_id grain_id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contenug'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Grains'), array('controller' => 'grains', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grain'), array('controller' => 'grains', 'action' => 'add')); ?> </li>
	</ul>
</div>
