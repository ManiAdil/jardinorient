<div>
	<h3>Liste des messages </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('contenu'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('lu'); ?></th>
   <th class="data"><?php echo $this->Paginator->sort('nom'); ?></th>
			<th  class="data">Actions</th>
	</tr>
	<?php
	foreach ($messages as $message): 
    if($message['Message']['lu']==0)
    {
    ?>
    <tr class="data" bgcolor="#f494a3">
    <?php }else{?>
        <tr class="data" bgcolor="#a3dfa6">
    <?php }?>
		<td class="data"><?php echo h($message['Message']['id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($message['Message']['contenu']); ?>&nbsp;</td>
		<td class="data"><?php if($message['Message']['lu']==0){?>Non<?php }else{?>Oui<?php }?>&nbsp;</td>
        <td class="data"><?php echo h($message['Message']['nom']); ?>&nbsp;</td>
		<td  class="data">
			<?php echo $this->Html->link(__('Afficher |'), array('action' => 'view', $message['Message']['id'])); ?>
					<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $message['Message']['id']), null, __('Etes vous sûr de vouloir supprimer le message : # %s?', $message['Message']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
