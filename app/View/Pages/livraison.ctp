<center><?php echo $this->Html->image("livraison.gif")?></center>


<u><h2>Combien coûte la livraison ?</h2></u>


<h5>La livraison est assurée en interne ou par nos prestataires. Les frais de livraison sont 
gratuits au-delà d’un certain montant de commande. Certains frais de livraison
 supplémentaires peuvent être engendrés pour certains produits (en fonction de leur poids ou de leur taille),
 ainsi que pour certaines localités.</h5>