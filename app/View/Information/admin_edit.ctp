
<h3>Modifier les informations de la pépinière</h3>

<?php echo $this->Form->create('Information'); ?>

<table width="100%">
  <tr><td>Présentation de la pépinière :</td><td><?php	echo $this->Form->input('presentation',array('label'=>''));?></td></tr>
  <tr><td>Adresse :</td><td><?php	echo $this->Form->input('adresse',array('label'=>''));?></td></tr>
  <tr><td>Telephone :</td><td><?php	echo $this->Form->input('tel',array('label'=>''));?></td></tr>
  <tr><td>Fix :</td><td>	<?php	echo $this->Form->input('fix',array('label'=>''));?></td></tr>
  <tr><td>Fax :</td><td><?php	echo $this->Form->input('fax',array('label'=>''));?></td></tr>
</table>

<?php echo $this->Form->end(__('Valider la modification')); ?>



