<div class="information view">
<h2><?php  echo __('Information'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($information['Information']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Presentation'); ?></dt>
		<dd>
			<?php echo h($information['Information']['presentation']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
			<?php echo h($information['Information']['adresse']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
			<?php echo h($information['Information']['tel']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fix'); ?></dt>
		<dd>
			<?php echo h($information['Information']['fix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Fax'); ?></dt>
		<dd>
			<?php echo h($information['Information']['fax']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Information'), array('action' => 'edit', $information['Information']['id'])); ?> </li>
		
	</ul>
</div>
