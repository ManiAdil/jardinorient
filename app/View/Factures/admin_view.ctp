<div class="factures view">
<h2><?php  echo __('Facture'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($facture['Facture']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prix'); ?></dt>
		<dd>
			<?php echo h($facture['Facture']['prix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?></dt>
		<dd>
			<?php echo h($facture['Facture']['total']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?></dt>
		<dd>
			<?php echo h($facture['Facture']['date']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Facture'), array('action' => 'edit', $facture['Facture']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Facture'), array('action' => 'delete', $facture['Facture']['id']), null, __('Are you sure you want to delete # %s?', $facture['Facture']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Factures'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facture'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('controller' => 'livraisons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Livraisons'); ?></h3>
	<?php if (!empty($facture['Livraison'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Facture Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($facture['Livraison'] as $livraison): ?>
		<tr>
			<td><?php echo $livraison['id']; ?></td>
			<td><?php echo $livraison['facture_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'livraisons', 'action' => 'view', $livraison['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'livraisons', 'action' => 'edit', $livraison['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'livraisons', 'action' => 'delete', $livraison['id']), null, __('Are you sure you want to delete # %s?', $livraison['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
