<div>
	<h3>Liste des factures </h3>
	<table class="data">
	<tr class="data">
		<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('prix'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('total'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('date'); ?></th>
		
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($factures as $facture):?>
	<tr>
			<td class="data"><?php echo h($facture['Facture']['id']); ?>&nbsp;</td>
			<td class="data"><?php echo h($facture['Facture']['prix']); ?>&nbsp;</td>
			<td class="data"><?php echo h($facture['Facture']['total']); ?>&nbsp;</td>
		<td class="data"><?php echo h($facture['Facture']['date']); ?>&nbsp;</td>

             <td  class="data">
			
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $facture['Facture']['id']), null, __('Are you sure you want to delete # %s?', $facture['Facture']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
