<div class="factures form">
<?php echo $this->Form->create('Facture'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Facture'); ?></legend>
	<?php
		echo $this->Form->input('prix');
		echo $this->Form->input('total');
		echo $this->Form->input('date');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Factures'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('controller' => 'livraisons', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('controller' => 'livraisons', 'action' => 'add')); ?> </li>
	</ul>
</div>
