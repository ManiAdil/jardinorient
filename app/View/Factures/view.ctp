<div class="factures view">
<p class="inscriptionf"><?php  echo __('Votre Facture :'); ?></p>
	<dl>
	
		<dt><?php echo __('Prix'); ?> :</dt>
		<dd>
			<?php echo h($facture['Facture']['prix']); ?><br>
			&nbsp;
		</dd>
		<dt><?php echo __('Total'); ?> :</dt>
		<dd>
			<?php echo h($facture['Facture']['total']); ?><br>
			&nbsp;
		</dd>
		<dt><?php echo __('Date'); ?> :</dt>
		<dd>
			<?php echo h($facture['Facture']['date']); ?><br>
			&nbsp;
		</dd>
	</dl>
</div>
