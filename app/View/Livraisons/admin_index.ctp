<div>
	<h3>Liste des utilisateurs </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('facture_id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('commande_id'); ?></th>
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($livraisons as $livraison):?>
	<tr>
			<td class="data"><?php echo h($livraison['Livraison']['id']); ?>&nbsp;</td>
			<td class="data"><?php echo h($livraison['Livraison']['facture_id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($livraison['Livraison']['commande_id']); ?>&nbsp;</td>
             <td  class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $livraison['Livraison']['id'])); ?>
			<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $livraison['Livraison']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $livraison['Livraison']['id']), null, __('Are you sure you want to delete # %s?', $livraison['Livraison']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>

