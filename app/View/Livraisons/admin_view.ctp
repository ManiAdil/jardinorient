<div class="livraisons view">
<h2><?php  echo __('Livraison'); ?></h2>
	<dl>
		<dt><?php echo __('Commande'); ?></dt>
		<dd>
			<?php echo $this->Html->link($livraison['Commande']['id'], array('controller' => 'commandes', 'action' => 'view', $livraison['Commande']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Facture'); ?></dt>
		<dd>
		
			<?php echo $this->Html->link($livraison['facture']['id'], array('controller' => 'factures', 'action' => 'view', $livraison['facture']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Commande Id'); ?></dt>
		<dd>
			<?php echo h($livraison['Livraison']['commande_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Livraison'), array('action' => 'edit', $livraison['Livraison']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Livraison'), array('action' => 'delete', $livraison['Livraison']['id']), null, __('Are you sure you want to delete # %s?', $livraison['Livraison']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Livraisons'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Livraison'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Factures'), array('controller' => 'factures', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Facture'), array('controller' => 'factures', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Paiements'), array('controller' => 'paiements', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Paiement'), array('controller' => 'paiements', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Paiements'); ?></h3>
	<?php if (!empty($livraison['paiement'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Date'); ?></th>
		<th><?php echo __('Livraison Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($livraison['paiement'] as $paiement): ?>
		<tr>
			<td><?php echo $paiement['id']; ?></td>
			<td><?php echo $paiement['date']; ?></td>
			<td><?php echo $paiement['livraison_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'paiements', 'action' => 'view', $paiement['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'paiements', 'action' => 'edit', $paiement['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'paiements', 'action' => 'delete', $paiement['id']), null, __('Are you sure you want to delete # %s?', $paiement['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Paiement'), array('controller' => 'paiements', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
