<html>
<head>
<title>Administration de : <?php echo $title_for_layout;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Copyright" content="arirusmanto.com">
<meta name="description" content="Admin MOS Template">
<meta name="keywords" content="Admin Page">
<meta name="author" content="Ari Rusmanto">
<meta name="language" content="Bahasa Indonesia">




<link rel="shortcut icon" href="stylesheet/img/devil-icon.png"/> <!--Pemanggilan gambar favicon-->

<?php echo $this->Html->css("admin-css/mos-style.css");?>  <!--pemanggilan file css-->




</head>

<body>
<div id="header">
	<div class="inHeader">
		<div class="mosAdmin">
		Bienvenu Mr.Admin<br>
		<a href="">Vous êtes connecté sur la partie d'administration</a>
        <a href="<?php echo $this->Html->url(array('controller'=>'users','action'=>'logout'))?>">déconnexion</a> 
		</div>
	<div class="clear"></div>
	</div>
</div>

<div id="wrapper">
	<div id="leftBar">
	<ul>




		<li><a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'admin','admin'=>false));?>">Acceuil</a></li>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'information','action'=>'edit/1','admin'=>true));?>">Vos Infos</a></li>
        <li><a href="<?php echo $this->Html->url(array('controller'=>'messages','action'=>'index','admin'=>true));?>">Messages</a></li>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'grains','action'=>'index','admin'=>true));?>">Grains</a></li>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'outils','action'=>'index','admin'=>true));?>">Outils</a></li>
       	<li><a href="<?php echo $this->Html->url(array('controller'=>'news','action'=>'index','admin'=>true));?>">Nouveautés</a></li>
	
		<li><a href="<?php echo $this->Html->url(array('controller'=>'villes','action'=>'index','admin'=>true))?>">Villes</a></li>
 	    <li><a href="<?php echo $this->Html->url(array('controller'=>'paiements','action'=>'index','admin'=>true))?>">Paiements</a></li>
 	    <li><a href="<?php echo $this->Html->url(array('controller'=>'clients','action'=>'index','admin'=>true))?>">Clients</a></li>

	</ul>
	</div>
	<div id="rightContent">
	<?php echo $content_for_layout?>
	</div>
</div>
<div class="clear"></div>
<div id="footer">
	Designed By : Groupe EST & Groupe ATDEV - 2013
</div>

</body>
</html>