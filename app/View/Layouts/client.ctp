<html>
<head>
<title>Administration de : <?php echo $title_for_layout;?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Copyright" content="arirusmanto.com">
<meta name="description" content="Admin MOS Template">
<meta name="keywords" content="Admin Page">
<meta name="author" content="Ari Rusmanto">
<meta name="language" content="Bahasa Indonesia">




<link rel="shortcut icon" href="stylesheet/img/devil-icon.png"/> <!--Pemanggilan gambar favicon-->

<?php echo $this->Html->css("admin-css/mos-style.css");?>  <!--pemanggilan file css-->




</head>

<body>
<div id="header">
	<div class="inHeader">
		<div class="mosAdmin">
		Bienvenu Mr.Admin<br>
		<a href="">Vous �tes connect� sur la partie d'administration</a> 
		</div>
	<div class="clear"></div>
	</div>
</div>

<div id="wrapper">
	<div id="leftBar">
	<ul>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'pages','action'=>'client','admin'=>false));?>">Acceuil</a></li>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'information','action'=>'index','admin'=>true));?>">Commandes</a></li>
        <li><a href="<?php echo $this->Html->url(array('controller'=>'messages','action'=>'index','admin'=>true));?>">Livraison</a></li>
		<li><a href="<?php echo $this->Html->url(array('controller'=>'grains','action'=>'index','admin'=>true));?>">Paiement</a></li>
		
	</ul>
	</div>
	<div id="rightContent">
	<?php echo $content_for_layout?>
	</div>
</div>
<div class="clear"></div>
<div id="footer">
	Designed By : Groupe EST & Groupe ATDEV - 2013
</div>

</body>
</html>