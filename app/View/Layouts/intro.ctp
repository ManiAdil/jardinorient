<html><!--Google Fonts--><head><link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css">

<!-- *************************************************************************
**************************   STYLE SHEET   *******************************
************************************************************************** -->
<?php echo $this->Html->css("main");?>

<?php echo $this->Html->css("jquery.fancybox");?>


<!--[if IE 7 ]>    <html class= "ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class= "ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class= "ie9"> <![endif]-->

<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
   </script>
<![endif]-->


<!-- Mirrored from mythemepreviews.com/nature-html/ by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 27 Mar 2013 11:39:43 GMT -->


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Welcome to Nature</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">


 <!-- *************************************************************************
*****************                FAVICON               ********************
************************************************************************** -->

<link rel="shortcut icon" href="img/favicon.png">

  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- *************************************************************************
**************************        JS       *******************************
************************************************************************** -->
<script type="text/javascript" src="js/jquery.js"> </script>
<script type="text/javascript" src="js/jquery.flexslider-min.js"> </script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"> </script>
<script type="text/javascript" src="js/hoverIntent.js"> </script>
<script type="text/javascript" src="js/respond.min.js"> </script>
<script type="text/javascript" src="js/jquery.sfmenu.js"> </script>
<script type="text/javascript" src="js/jquery.preloadcssimages.js"> </script>


 <!-- *************************************************************************
*****************        RESPONSIVE MENU SELECT        ********************
************************************************************************** -->
<script type="text/javascript">
// DOM ready
jQuery(document).ready(function(){

// Create the dropdown base
jQuery("<select />").appendTo("#topmenu");

// Create default option "Go to..."
jQuery("<option />", {
 "selected": "selected",
 "value"   : "",
 "text"    : "Menu Selection..."
}).appendTo("#topmenu select");

// Populate dropdown with menu items
jQuery("#topmenu a").each(function() {
var el = jQuery(this);
jQuery("<option />", {
   "value"   : el.attr("href"),
   "text"    : el.text()
}).appendTo("#topmenu select");
});

// To make dropdown actually work
// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
jQuery("#topmenu select").change(function() {
window.location = jQuery(this).find("option:selected").val();
});

});
</script>



 <!-- *************************************************************************
*****************              TWITTER FEED            ********************
************************************************************************** -->

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(".tweet").tweet({
            username: "@twsjonathan",
            join_text: "auto",
            avatar_size: 32,
            count: 3,
            auto_join_text_default: "we said,", 
            auto_join_text_ed: "we",
            auto_join_text_ing: "we were",
            auto_join_text_reply: "we replied to",
            auto_join_text_url: "we were checking out",
            loading_text: "loading tweets..."
        });
    });
</script>



<!-- *************************************************************************
******************   THIS IS THE SLIDER           ***********************
************************************************************************** -->
<script type="text/javascript">
jQuery(window).load(function() {
	jQuery('.slider').flexslider();
});
	
</script>


</head>

<body>

<!-- Start of outer wrapper -->
<header id="outer_wrapper">

<!-- Start of top message area -->
<!-- End of top message area -->

</header><!-- End of outer wrapper -->

<!-- Start of bdywrapper -->
<div class="bdywrapper">

<!-- Start of nav wrapper -->
<div id="nav_wrapper">

<!-- Start of top logo -->
<div id="top_logo">
<a href="#"><img src="img/top_logo.png" alt="logo"></a>

</div><!-- End of top logo -->



<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of nav wrapper --> 

<!-- Start of topmenu wrapper -->
<div id="topmenu_wrapper">

<!-- Start of topmenu -->
<nav id="topmenu">  
<center>
<ul class="sf-menu sf-js-enabled sf-shadow">

<li><a class="active" href="">Acceuil</a></li>
        
<li><a href="presentation">Presentation</a></li>


</li><li><a href="#" class="sf-with-ul">Produits<span class="sf-sub-indicator"> »</span></a>

<ul class="sub-menu" style="float: none; width: 14em; display: none; visibility: hidden; ">
<li style="float: left; width: 100%; white-space: normal; "><a href="leftsidebar.html" style="float: none; width: auto; ">Arbres</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="rightsidebar.html" style="float: none; width: auto; ">Arbustes</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="fullwidth.html" style="float: none; width: auto; ">Rosiers</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="error.html" style="float: none; width: auto; ">Grains</a></li>

</ul>

</li>



<li><a href="photos.html">Panier</a></li>

</li><li><a href="#" class="sf-with-ul">Services<span class="sf-sub-indicator"> »</span></a>

<ul class="sub-menu" style="float: none; width: 14em; display: none; visibility: hidden; ">
<li style="float: left; width: 100%; white-space: normal; "><a href="livraison" style="float: none; width: auto; ">Livrasion</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="conseils" style="float: none; width: auto; ">Conseils</a></li>


</ul>

</li>

</li><li><a href="#" class="sf-with-ul">Contact<span class="sf-sub-indicator"> »</span></a>

<ul class="sub-menu" style="float: none; width: 14em; display: none; visibility: hidden; ">
<li style="float: left; width: 100%; white-space: normal; "><a href="contact" style="float: none; width: auto; ">Contactez nous</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="localisation" style="float: none; width: auto; ">Localisation</a></li>
<li style="float: left; width: 100%; white-space: normal; "><a href="coordonnees" style="float: none; width: auto; ">Coordonnees</a></li>


</ul>

</li>


</ul>
<!-- Start of clear fix --><div class="clear"></div>

<select><option selected="selected" value="">Menu Selection...</option><option value="index-2.html">Home</option><option value="index2.html">Home - Alternative</option><option value="about.html">About Us</option><option value="events.html">Events</option><option value="#">Pages »</option><option value="leftsidebar.html">Left Sidebar</option><option value="rightsidebar.html">Right Sidebar</option><option value="fullwidth.html">Fullwidth Page</option><option value="error.html">404 Page</option><option value="columns.html">Columns</option><option value="styles.html">Styles</option><option value="blog.html">Blog »</option><option value="blog_left.html">Left Sidebar</option><option value="blog_right.html">Right Sidebar</option><option value="blog_fullwidth.html">Fullwidth Sidebar</option><option value="photos.html">Photos</option><option value="contact.html">Contact</option></select></nav><!-- End of topmenu -->
</center>
</div><!-- End of topmenu wrapper -->

</div><!-- End of bdywrapper -->

<!-- Start of midsectionhr --><div id="midsectionhr"></div>

<!-- Start of bdywrapper -->
<div class="bdywrapper">

<!-- ******************************************************************** This is the slider ********************************************************************-->

<!-- Start of slider wrapper -->
<section class="slider_wrapper">

<!-- Start of slider -->
<section class="slider"> &nbsp;&nbsp;

<ul class="slides">

<li style="width: 100%; float: left; margin-right: -100%; display: list-item; "><a href="#" target="_new" title="Slide Title 1"><img src="img/abeille.jpg" alt="slide 1"></a></li>
<li style="width: 100%; float: left; margin-right: -100%; display: none; "><a href="#" target="_new" title="Slide Title 2"><img src="img/fleur1.jpg" alt="slide 2"></a></li>
<li style="width: 100%; float: left; margin-right: -100%; display: none; "><a href="#" target="_new" title="Slide Title 3"><img src="img/jasmin1.jpg" alt="slide 3"></a></li>
<li style="width: 100%; float: left; margin-right: -100%; display: none; "><a href="#" target="_new" title="Slide Title 4"><img src="img/arbustes1.jpg" alt="slide 4"></a></li>
    
</ul>
    
<ol class="flex-control-nav"><li><a class="active">1</a></li><li><a class="">2</a></li><li><a class="">3</a></li><li><a class="">4</a></li></ol></section><!-- End of slider -->

<!-- Start of clear fix --><div class="clear"></div>

</section><!-- End of slider wrapper -->








<!-- ******************************************************************** This is the message area under the slider ********************************************************************-->

<!-- Start of main section home -->
<section id="main_section_home">

<!-- Start of message center -->
<section id="message_center">

<!-- Start of message center left -->
<div class="message_center_left">
<h1 class="homepage">Qui somme nous !</h1>

<p>  Créée en 2012 , Jardin d’Orient est une pépinière située
     à proximité de la ville d’Oujda, d’une surface de 35 hectares, 

     des arbres et arbustes de pleine terre. La production de cette 
     jardinerie se situe dans des champs à l'air libre .....</p>

<!-- Start of under slider button -->
<div class="under_slider_button">
<a href="presentation">Lire la suite</a>

</div><!-- End of under slider button -->

</div><!-- End of message center left -->

<!-- Start of message center right -->
<!-- End of donatetitle -->

<!-- Start of donateamount -->


<!-- End of donateamount -->

<!-- Start of meterwrapper -->


<!-- Start of meter -->


<!-- End of meter -->

<!-- End of meterwrapper -->

<!-- Start of donatetitle -->


<!-- End of donatetitle -->

<!-- Start of donateamount -->



<!-- End of donateamount -->

<!-- End of message center right -->

<!-- Start of clear fix<!-- End of message center -->








<!-- ******************************************************************** This is the blog loop under the message center  - left side ********************************************************************-->



<a><img src="img/livraison.jpg"></a>

<!-- End of message center right -->

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->







<!-- ******************************************************************** This is homepage newsletter signup ********************************************************************-->










<!-- ******************************************************************** This is start of the footer ********************************************************************-->

<!-- Start of top -->
<div id="top"> 

<!-- Start of main section bg -->
<div id="main_section_bg">


</div><!-- End of main section bg -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of top -->

<!-- Start of outer footer wrapper -->
<footer id="outer_footer_wrapper">

<!-- Start of footer wrapper -->
<div id="footer_wrapper">

<!-- Start of one fourth first -->
<div class="one_fourth_first">


<h6>Sites Amis</h6>
<ul>
<li><a href="#">Pepiniére casa</a></li>
<li><a href="#">jardin du paradis</a></li>
<li><a href="#">Vert espace</a></li>
<li><a href="#">Goût de nature</a></li>

</ul>

</div><!-- end of one fourth first -->




<!-- end of one fourth -->

<!-- Start of one fourth -->



<!-- end of one fourth -->

<!-- Start of one fourth -->
<!-- end of one fourth -->

</div><!-- End of footer wrapper -->

<!-- Start of clear fix --><div class="clear"></div>

</footer><!-- End of outer footer wrapper -->

<!-- Start of bottom nav wrap -->
<div id="bottom_nav_wrap">

<!-- Start of copyright message -->
<div class="copyright_message"> 


</div><!-- End of copyright message -->

<!-- Start of social -->
<section class="social">

<ul class="icons">

<li><a href="#"><img src="img/pinterest.png" height="18" width="13" alt="pinterest"></a></li>

<li><a href="#"><img src="img/flickr.png" height="18" width="20" alt="flickr"></a></li>

<li><a href="#"><img src="img/googleplus.png" height="15" width="16" alt="google plus"></a></li>

<li><a href="#"><img src="img/twitter.png" height="13" width="17" alt="twitter"></a></li>

<li><a href="#"><img src="img/facebook.png" height="18" width="9" alt="facebook"></a></li>

</ul>

</section><!-- End of social -->

</div><!-- end of bottom nav wrap -->

<div class="clear"></div>



<!-- Mirrored from mythemepreviews.com/nature-html/ by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 27 Mar 2013 11:40:54 GMT -->

</body></html>

