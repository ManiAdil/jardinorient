<html><!--Google Fonts--><link href="http://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css">
<head>

<!-- *************************************************************************
**************************   STYLE SHEET   *******************************
************************************************************************** -->
<?php echo $this->Html->css("main");?>
<?php echo $this->Html->css("jquery.fancybox");?>



<!--[if IE 7 ]>    <html class= "ie7"> <![endif]-->
<!--[if IE 8 ]>    <html class= "ie8"> <![endif]-->
<!--[if IE 9 ]>    <html class= "ie9"> <![endif]-->

<!--[if lt IE 9]>
   <script>
      document.createElement('header');
      document.createElement('nav');
      document.createElement('section');
      document.createElement('article');
      document.createElement('aside');
      document.createElement('footer');
   </script>
<![endif]-->


<!-- Mirrored from mythemepreviews.com/nature-html/rightsidebar.html by HTTrack Website Copier/3.x [XR&CO'2010], Wed, 27 Mar 2013 11:41:08 GMT -->


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Welcome to Nature</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">


 <!-- *************************************************************************
*****************                FAVICON               ********************
************************************************************************** -->

<link rel="shortcut icon" href="img/favicon.png">

  <!--[if lt IE 9]>
<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- *************************************************************************
**************************        JS       *******************************
************************************************************************** -->
<?php echo $this->Html->script("jquery"); ?>
<?php echo $this->Html->script("jquery.easing.1.3"); ?>
<?php echo $this->Html->script("hoverIntent"); ?>
<?php echo $this->Html->script("respond.min"); ?>
<?php echo $this->Html->script("jquery.sfmenu"); ?>
<?php echo $this->Html->script("jquery.preloadcssimages"); ?>
<?php echo $this->Html->script("jquery.fancybox"); ?>
<?php echo $this->Html->script("jquery.tweet"); ?>
<?php echo $this->Html->script("custom"); ?>





<!-- *************************************************************************
*****************            LIGHTBOX SCRIPT           ********************
************************************************************************** -->

<script type="text/javascript">
jQuery(document).ready(function() {
jQuery('.fancybox').fancybox();
});
</script>


 <!-- *************************************************************************
*****************        RESPONSIVE MENU SELECT        ********************
************************************************************************** -->
<script type="text/javascript">
// DOM ready
jQuery(document).ready(function(){

// Create the dropdown base
jQuery("<select />").appendTo("#topmenu");

// Create default option "Go to..."
jQuery("<option />", {
 "selected": "selected",
 "value"   : "",
 "text"    : "Menu Selection..."
}).appendTo("#topmenu select");

// Populate dropdown with menu items
jQuery("#topmenu a").each(function() {
var el = jQuery(this);
jQuery("<option />", {
   "value"   : el.attr("href"),
   "text"    : el.text()
}).appendTo("#topmenu select");
});

// To make dropdown actually work
// To make more unobtrusive: http://css-tricks.com/4064-unobtrusive-page-changer/
jQuery("#topmenu select").change(function() {
window.location = jQuery(this).find("option:selected").val();
});

});
</script>	



 <!-- *************************************************************************
*****************              TWITTER FEED            ********************
************************************************************************** -->

<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery(".tweet").tweet({
            username: "@twsjonathan",
            join_text: "auto",
            avatar_size: 32,
            count: 3,
            auto_join_text_default: "we said,", 
            auto_join_text_ed: "we",
            auto_join_text_ing: "we were",
            auto_join_text_reply: "we replied to",
            auto_join_text_url: "we were checking out",
            loading_text: "loading tweets..."
        });
    });
</script>

<script type="text/javascript">
jQuery(window).load(function() {
	jQuery('.slider').flexslider();
});
	
</script>





</head>

<body>

<!-- Start of outer wrapper -->
<!-- End of outer wrapper -->

<!-- Start of bdywrapper -->
<div class="bdywrapper">

<!-- Start of nav wrapper -->
<div id="nav_wrapper">

<!-- Start of top logo -->
<div id="top_logo">
<a href="#"><?php echo $this->Html->image("top_logo.png"); ?></a>

</div><!-- End of top logo -->



<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of nav wrapper --> 

<!-- Start of topmenu wrapper -->
<div id="topmenu_wrapper">

<!-- Start of topmenu -->

<nav id="topmenu">  
<ul class="sf-menu sf-js-enabled sf-shadow">

<li><a href="#">Accueil</a></li>
        
<li><a href="#">Presentation</a></li>
</li>

<?php echo $this->element('/menu'); ?>



<li><a href="#">Panier</a></li>

<li><a class="active sf-with-ul" href="#">Services</a><span class="sf-sub-indicator"> �</span></a>

<ul class="sub-menu" style="float: none; width: 14em; display: none; visibility: hidden;">
<li style="white-space: normal; float: left; width: 100%;"><a href="#" style="float: none; width: auto;">Conseils</a></li>
<li style="white-space: normal; float: left; width: 100%;"><a class="active" href="#" style="float: none; width: auto;">Livraison</a></li>

</ul>



<li><a href="#" class="sf-with-ul">Contact<span class="sf-sub-indicator"> �</span></a>

<ul class="sub-menu" style="float: none; width: 14em; display: none; visibility: hidden;">
<li style="white-space: normal; float: left; width: 100%;"><a href="#" style="float: none; width: auto;">Cordonnees</a></li>
<li style="white-space: normal; float: left; width: 100%;"><a href="#" style="float: none; width: auto;">Localisation</a></li>
<li style="white-space: normal; float: left; width: 100%;"><a href="#" style="float: none; width: auto;">Contactez-nous</a></li>
</ul>

</li>



</ul>

<!-- Start of clear fix --><div class="clear"></div>

<select><option selected="selected" value="">Menu Selection...</option><option value="index-2.html">Home</option><option value="index2.html">Home - Alternative</option><option value="about.html">About Us</option><option value="events.html">Events</option><option value="#">Pages �</option><option value="leftsidebar.html">Left Sidebar</option><option value="rightsidebar.html">Right Sidebar</option><option value="fullwidth.html">Fullwidth Page</option><option value="error.html">404 Page</option><option value="columns.html">Columns</option><option value="styles.html">Styles</option><option value="blog.html">Blog �</option><option value="blog_left.html">Left Sidebar</option><option value="blog_right.html">Right Sidebar</option><option value="blog_fullwidth.html">Fullwidth Sidebar</option><option value="photos.html">Photos</option><option value="contact.html">Contact</option></select></nav><!-- End of topmenu -->

</div><!-- End of topmenu wrapper -->

</div><!-- End of bdywrapper -->

<!-- Start of midsectionhr --><div id="midsectionhr"></div>

<!-- Start of bdywrapper -->
<div class="bdywrapper"> 

<!-- Start of main -->
<section id="main">
<article style="width: 65%; float: left;"><?php echo $content_for_layout?></article>


<!-- Start of message center left -->
<!-- End of message center left -->

<?php echo $this->element('/widgets'); ?>

<!-- Start of clear fix --><div class="clear"></div>
            
</section><!-- End of main -->






<!-- ******************************************************************** This is homepage newsletter signup ********************************************************************-->


<!-- Start of home newsletter -->
<div class="homenewsletter">
<form action="http://mythemepreviews.com/nature-html/yourscript.php" method="post" id="contactform">
<span class="newslettertext">Sign up for our e-newsletter!</span> <input type="text" size="40" value="" name="your-email">
<input class="news-submit" type="submit" value="Send">
</form>

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of home newsletter -->

</div><!-- End of bdywrapper -->





<!-- ******************************************************************** This is start of the footer ********************************************************************-->

<!-- Start of top -->
<div id="top"> 

<!-- Start of main section bg -->
<div id="main_section_bg">


</div><!-- End of main section bg -->

<!-- Start of clear fix --><div class="clear"></div>

</div><!-- End of top -->

<!-- Start of outer footer wrapper -->
<footer id="outer_footer_wrapper">

<!-- Start of footer wrapper -->
<div id="footer_wrapper">

<!-- Start of one fourth first -->
<div class="one_fourth_first">

<a href="#"><img src="img/bottom_logo.png" alt="bottom logo"></a>

<p>is a nonprofit, tax-exempt charitable organization under Section 501(c)(3) of the Internal Revenue Code. Donations are tax-deductible as allowed by law.</p>

</div><!-- end of one fourth first -->

<!-- Start of one fourth -->
<div class="one_fourth">
<a href="#"><img src="img/footerimage.png" alt="go daddy verified"></a>

</div><!-- end of one fourth -->

<!-- Start of one fourth -->
<div class="one_fourth">
<h6>About Us</h6>
<ul>
<li><a href="#">About Us</a></li>
<li><a href="#">Blog</a></li>
<li><a href="#">News</a></li>
<li><a href="#">Vision and Mission</a></li>
<li><a href="#">Our Accountability</a></li>
<li><a href="#">Financials</a></li>
</ul>

</div><!-- end of one fourth -->

<!-- Start of one fourth -->
<div class="one_fourth">
<h6>Our Friends</h6>
<ul>
<li><a href="#">WWF</a></li>
<li><a href="#">Redcross.org</a></li>
<li><a href="#">Charity : Water</a></li>
<li><a href="#">Make A Wish Foundation</a></li>
<li><a href="#">Nature.org</a></li>
<li><a href="#">ASPCA.org</a></li>
</ul>

</div><!-- end of one fourth -->

</div><!-- End of footer wrapper -->

<!-- Start of clear fix --><div class="clear"></div>

</footer><!-- End of outer footer wrapper -->

<!-- Start of bottom nav wrap -->
<div id="bottom_nav_wrap">

<!-- Start of copyright message -->
<div class="copyright_message"> 
� 2012 Jonathan Atkinson

</div><!-- End of copyright message -->

<!-- Start of social -->
<section class="social">

<ul class="icons">

<li><a href="#"><img src="img/pinterest.png" height="18" width="13" alt="pinterest"></a></li>

<li><a href="#"><img src="img/flickr.png" height="18" width="20" alt="flickr"></a></li>

<li><a href="#"><img src="img/googleplus.png" height="15" width="16" alt="google plus"></a></li>

<li><a href="#"><img src="img/twitter.png" height="13" width="17" alt="twitter"></a></li>

<li><a href="#"><img src="img/facebook.png" height="18" width="9" alt="facebook"></a></li>

</ul>

</section><!-- End of social -->

</div><!-- end of bottom nav wrap -->

<div class="clear"></div>


</body></html>