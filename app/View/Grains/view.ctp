<div class="grains view">
<h2><?php  echo __('Grain'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['nom']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Definition'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['definition']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prix'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['prix']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantite'); ?></dt>
		<dd>
			<?php echo h($grain['Grain']['quantite']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Grain'), array('action' => 'edit', $grain['Grain']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Grain'), array('action' => 'delete', $grain['Grain']['id']), null, __('Are you sure you want to delete # %s?', $grain['Grain']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Grains'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grain'), array('action' => 'add')); ?> </li>
	</ul>
</div>
