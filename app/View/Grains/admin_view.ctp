<div class="grains view">
<h2><?php  echo __('Grain'); ?></h2>
	<dl>
		<dt><h4><u><?php echo __('Id:'); ?></h4></u></dt>
		<dd>
			<?php echo h($grain['Grain']['id']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Nom:'); ?></h4></u></dt>
		<dd>
			<?php echo h($grain['Grain']['nom']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Definition:'); ?></h4></u></dt>
		<dd>
			<?php echo h($grain['Grain']['definition']); ?>
			&nbsp;
		</dd>
		
		<dt></a><u><h4><?php echo __('Prix:'); ?></u></h4></dt>
		<dd>
			<?php echo h($grain['Grain']['prix']); ?>
			&nbsp;
		</dd>
		<dt><u><h4><?php echo __('Quantite:'); ?></u></h4></dt>
		<dd>
			<?php echo h($grain['Grain']['quantite']); ?>
			&nbsp;
		</dd>
        <dt><u><h4><?php echo __('Image:'); ?></u></h4></dt>
		<dd>
			<?php echo $this->Html->image("upload/".$grains['Grain']['image']);?>
            &nbsp;


		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Grain'), array('action' => 'edit', $grain['Grain']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Grain'), array('action' => 'delete', $grain['Grain']['id']), null, __('Are you sure you want to delete # %s?', $grain['Grain']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Grains'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Grain'), array('action' => 'add')); ?> </li>
	</ul>
</div>
