<div class="grains form">
<?php echo $this->Form->create('Grain',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Grain'); ?></legend>
	<?php
		echo $this->Form->input('nom');
		echo $this->Form->input('definition');
		echo $this->Form->input('Grain.image',array('type'=>'file','error'=>array('attributes'=>array('wrap'=>'span','class'=>'help-inline error')),'label'=>false,"class"=>"span")); 
		echo $this->Form->input('prix');
		echo $this->Form->input('quantite');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Grains'), array('action' => 'index')); ?></li>
	</ul>
</div>
