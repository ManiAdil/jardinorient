<div>
	<h3>Liste des plantes </h3>
	<table class="data">
	<tr class="data">
    
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('nom'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('definition'); ?></th>
		<th class="data"><?php echo $this->Paginator->sort('image'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('prix'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('quantite'); ?></th>
			<th class="data"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($grains as $grain): ?>
	<tr>
		<td class="data"><?php echo h($grain['Grain']['id']); ?>&nbsp;</td>
		<td class="data"><?php echo h($grain['Grain']['nom']); ?>&nbsp;</td>
	<td class="data"><?php echo h($grain['Grain']['definition']); ?>&nbsp;</td>
		<td class="data"><?php echo h($grain['Grain']['image']); ?>&nbsp;</td>
		<td class="data"><?php echo h($grain['Grain']['prix']); ?>&nbsp;</td>
		<td class="data"><?php echo h($grain['Grain']['quantite']); ?>&nbsp;</td>
		<td class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $grain['Grain']['id'])); ?>
			<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $grain['Grain']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $grain['Grain']['id']), null, __('Are you sure you want to delete # %s?', $grain['Grain']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => '  '));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
