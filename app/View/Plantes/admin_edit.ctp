<div class="plantes form">
<?php echo $this->Form->create('Plante',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Plante'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nom');
		echo $this->Form->input('definition');
		echo $this->Form->input('caracteristiques');
		echo $this->Form->input('prix');
		echo $this->Form->input('quantite');
		echo $this->Form->input('famille_id');
		echo $this->Form->input('secteur_id');
         echo $this->Form->input('Plante.image',array('type'=>'file','error'=>array('attributes'=>array('wrap'=>'span','class'=>'help-inline error')),'label'=>false,"class"=>"span")); 
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Plante.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Plante.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Familles'), array('controller' => 'familles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Famille'), array('controller' => 'familles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Secteurs'), array('controller' => 'secteurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Secteur'), array('controller' => 'secteurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
