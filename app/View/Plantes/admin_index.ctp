<div>
	<h3>Liste des plantes </h3>
	<table class="data">
	<tr class="data">
			
          	<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('nom'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('definition'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('caracteristiques'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('image'); ?></th>
		<th class="data"><?php echo $this->Paginator->sort('prix'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('quantite'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('famille_id'); ?></th>
		<th class="data"><?php echo $this->Paginator->sort('secteur_id'); ?></th>
		
   
  <th  class="data">Actions</th>
	</tr>
		<?php foreach ($plantes as $plante): ?>
	<tr>
		
      <td class="data"><?php echo h($plante['Plante']['id']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['nom']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['definition']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['caracteristiques']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['image']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['prix']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['quantite']); ?>&nbsp;</td>
      <td class="data"><?php echo h($plante['Plante']['famille_id']); ?>&nbsp;</td>
	  <td class="data"><?php echo h($plante['Plante']['secteur_id']); ?>&nbsp;</td>
		<td  class="data">
			<?php echo $this->Html->link(__('Afficher |'), array('action' => 'view', $plante['Plante']['id'])); ?>
            <?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $plante['Plante']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $plante['Plante']['id']), null, __('Etes vous s�r de vouloir supprimer le message : # %s?',  $plante['Plante']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , D�part {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Pr�cedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
