<div class="plantes view">
<h2><?php  echo __('Plante'); ?></h2>
	<dl>
		<dt><h4><u><?php echo __('Id:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['id']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Nom:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['nom']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Definition:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['definition']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Caracteristiques:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['caracteristiques']); ?>
			&nbsp;
		</dd>
		
		<dt><h4><u><?php echo __('Prix:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['prix']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Quantite:'); ?></h4></u></dt>
		<dd>
			<?php echo h($plante['Plante']['quantite']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Famille:'); ?></h4></u></dt>
		<dd>
			<?php echo $this->Html->link($plante['Famille']['id'], array('controller' => 'familles', 'action' => 'view', $plante['Famille']['id'])); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Secteur:'); ?></h4></u></dt>
		<dd>
			<?php echo $this->Html->link($plante['Secteur']['id'], array('controller' => 'secteurs', 'action' => 'view', $plante['Secteur']['id'])); ?>
			&nbsp;
		</dd>
        
        <dt><h4><u><?php echo __('Image:'); ?></h4></u></dt>
		<dd>
			<?php echo $this->Html->image("upload/".$plante['Plante']['image']);

  ?>
			&nbsp;
		</dd>
        
        
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Plante'), array('action' => 'edit', $plante['Plante']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Plante'), array('action' => 'delete', $plante['Plante']['id']), null, __('Are you sure you want to delete # %s?', $plante['Plante']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Familles'), array('controller' => 'familles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Famille'), array('controller' => 'familles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Secteurs'), array('controller' => 'secteurs', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Secteur'), array('controller' => 'secteurs', 'action' => 'add')); ?> </li>
	</ul>
</div>
