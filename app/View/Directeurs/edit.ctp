<div class="directeurs form">
<?php echo $this->Form->create('Directeur'); ?>
	<fieldset>
		<legend><?php echo __('Edit Directeur'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('nom');
		echo $this->Form->input('prenom');
		echo $this->Form->input('mot_de_Directeur');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Directeur.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Directeur.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Directeurs'), array('action' => 'index')); ?></li>
	</ul>
</div>
