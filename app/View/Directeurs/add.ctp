<div class="directeurs form">
<?php echo $this->Form->create('Directeur'); ?>
	<fieldset>
		<legend><?php echo __('Add Directeur'); ?></legend>
	<?php
		echo $this->Form->input('nom');
		echo $this->Form->input('prenom');
		echo $this->Form->input('mot_de_Directeur');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Directeurs'), array('action' => 'index')); ?></li>
	</ul>
</div>
