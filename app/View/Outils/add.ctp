<div class="outils form">
<?php echo $this->Form->create('Outil'),array('type'=>'file'); ?>
	<fieldset>
		<legend><?php echo __('Add Outil'); ?></legend>
	<?php
		echo $this->Form->input('nom_o');
		echo $this->Form->input('definition_o');
		echo $this->Form->input('News.image',array('type'=>'file','error'=>array('attributes'=>array('wrap'=>'span','class'=>'help-inline error')),'label'=>false,"class"=>"span")); 
		echo $this->Form->input('prix_o');
		echo $this->Form->input('quantite_o');
		echo $this->Form->input('typesoutil_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Outils'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('controller' => 'typesoutils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('controller' => 'typesoutils', 'action' => 'add')); ?> </li>
	</ul>
</div>
