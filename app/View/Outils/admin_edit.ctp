<div class="outils form">
<?php echo $this->Form->create('Outil'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Outil'); ?></legend>
	<?php
		echo $this->Form->input('id_o');
		echo $this->Form->input('nom_o');
		echo $this->Form->input('definition_o');
		echo $this->Form->input('image');
		echo $this->Form->input('prix_o');
		echo $this->Form->input('quantite_o');
		echo $this->Form->input('typesoutil_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Outil.id_o')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Outil.id_o'))); ?></li>
		<li><?php echo $this->Html->link(__('List Outils'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('controller' => 'typesoutils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('controller' => 'typesoutils', 'action' => 'add')); ?> </li>
	</ul>
</div>
