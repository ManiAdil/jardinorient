<div class="outils index">
	<h2><?php echo __('Outils'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id_o'); ?></th>
			<th><?php echo $this->Paginator->sort('nom_o'); ?></th>
			<th><?php echo $this->Paginator->sort('definition_o'); ?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('prix_o'); ?></th>
			<th><?php echo $this->Paginator->sort('quantite_o'); ?></th>
			<th><?php echo $this->Paginator->sort('typesoutil_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($outils as $outil): ?>
	<tr>
		<td><?php echo h($outil['Outil']['id_o']); ?>&nbsp;</td>
		<td><?php echo h($outil['Outil']['nom_o']); ?>&nbsp;</td>
		<td><?php echo h($outil['Outil']['definition_o']); ?>&nbsp;</td>
		<td><?php echo h($outil['Outil']['image']); ?>&nbsp;</td>
		<td><?php echo h($outil['Outil']['prix_o']); ?>&nbsp;</td>
		<td><?php echo h($outil['Outil']['quantite_o']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($outil['Typesoutil']['id'], array('controller' => 'typesoutils', 'action' => 'view', $outil['Typesoutil']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $outil['Outil']['id_o'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $outil['Outil']['id_o'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $outil['Outil']['id_o']), null, __('Are you sure you want to delete # %s?', $outil['Outil']['id_o'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Outil'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('controller' => 'typesoutils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('controller' => 'typesoutils', 'action' => 'add')); ?> </li>
	</ul>
</div>
