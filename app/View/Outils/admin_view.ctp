<div class="outils view">
<h2><?php  echo __('Outil'); ?></h2>
	<dl>
		<dt><?php echo __('Id O'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['id_o']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom O'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['nom_o']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Definition O'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['definition_o']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Image'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['image']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Prix O'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['prix_o']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantite O'); ?></dt>
		<dd>
			<?php echo h($outil['Outil']['quantite_o']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Typesoutil'); ?></dt>
		<dd>
			<?php echo $this->Html->link($outil['Typesoutil']['id'], array('controller' => 'typesoutils', 'action' => 'view', $outil['Typesoutil']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Outil'), array('action' => 'edit', $outil['Outil']['id_o'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Outil'), array('action' => 'delete', $outil['Outil']['id_o']), null, __('Are you sure you want to delete # %s?', $outil['Outil']['id_o'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Outils'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Outil'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Typesoutils'), array('controller' => 'typesoutils', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Typesoutil'), array('controller' => 'typesoutils', 'action' => 'add')); ?> </li>
	</ul>
</div>
