
<div class="villes index">
	<p class="inscriptionf"><?php echo __('Villes'); ?> :</p>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('nom'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($villes as $ville): ?>
	<tr>
		<td><?php echo h($ville['Ville']['id']); ?>&nbsp;</td>
		<td><?php echo h($ville['Ville']['nom']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $ville['Ville']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $ville['Ville']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $ville['Ville']['id']), null, __('Are you sure you want to delete # %s?', $ville['Ville']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	
</div>
