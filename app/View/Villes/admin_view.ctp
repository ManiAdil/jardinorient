<div class="villes view">
<h2><?php  echo __('Ville'); ?></h2>
	<dl>

		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['nom']); ?>

		<dt><?php echo __('Id Ville'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['id_ville']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ville'); ?></dt>
		<dd>
			<?php echo h($ville['Ville']['ville']); ?>

			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('Edit Ville'), array('action' => 'edit', $ville['Ville']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ville'), array('action' => 'delete', $ville['Ville']['id']), null, __('Are you sure you want to delete # %s?', $ville['Ville']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Clients'); ?></h3>
	<?php if (!empty($ville['Client'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Nom'); ?></th>
		<th><?php echo __('Prenom'); ?></th>
		<th><?php echo __('Adresse'); ?></th>
		<th><?php echo __('Tel'); ?></th>
		<th><?php echo __('Ville Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($ville['Client'] as $client): ?>
		<tr>
			<td><?php echo $client['id']; ?></td>
			<td><?php echo $client['nom']; ?></td>
			<td><?php echo $client['prenom']; ?></td>
			<td><?php echo $client['adresse']; ?></td>
			<td><?php echo $client['tel']; ?></td>
			<td><?php echo $client['ville_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'clients', 'action' => 'view', $client['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'clients', 'action' => 'edit', $client['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'clients', 'action' => 'delete', $client['id']), null, __('Are you sure you want to delete # %s?', $client['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

		<li><?php echo $this->Html->link(__('Edit Ville'), array('action' => 'edit', $ville['Ville']['id_ville'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Ville'), array('action' => 'delete', $ville['Ville']['id_ville']), null, __('Are you sure you want to delete # %s?', $ville['Ville']['id_ville'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville'), array('action' => 'add')); ?> </li>
	</ul>
</div>

