<div>
	<h3>Liste des familles </h3>
	<table class="data">
	<tr class="data">
			
            <th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('famille'); ?></th>
         
         <th  class="data">Actions</th>
	</tr>
		<?php foreach ($familles as $famille): ?>
	<tr>
			
           <td class="data"><?php echo h($famille['Famille']['id']); ?>&nbsp;</td>
           <td class="data"><?php echo h($famille['Famille']['famille']); ?>&nbsp;</td>
        
        
             <td  class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $famille['Famille']['id'])); ?>
		   	<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $famille['Famille']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $famille['Famille']['id']), null, __('Are you sure you want to delete # %s?', $famille['Famille']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , D�part {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Pr�cedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p> 
    <div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Famille'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>




</div>
