<div class="familles view">
<h2><?php  echo __('Famille'); ?></h2>
	<dl>
	 <dt><h4><u><?php echo __('Id:'); ?></h4></u></dt>
		<dd>
			<?php echo h($famille['Famille']['id']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Famille:'); ?></h4></u></dt>
		<dd>
			<?php echo h($famille['Famille']['famille']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Famille'), array('action' => 'edit', $famille['Famille']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Famille'), array('action' => 'delete', $famille['Famille']['id']), null, __('Are you sure you want to delete # %s?', $famille['Famille']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Familles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Famille'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Plantes relatives'); ?></h3>
	<?php if (!empty($famille['Plante'])): ?>
	<table class="data">
	<tr class="data">
		<th class="data"><?php echo ('Id'); ?></th>
		<th class="data"><?php echo __('Nom'); ?></th>
		<th class="data"><?php echo __('Definition'); ?></th>
		<th class="data"><?php echo __('Caracteristiques'); ?></th>
		<th class="data"><?php echo __('image'); ?></th>
		<th class="data"><?php echo __('Prix'); ?></th>
		<th class="data"><?php echo __('Quantite'); ?></th>
		<th class="data"><?php echo __('Famille Id'); ?></th>
		<th class="data"><?php echo __('Secteur Id'); ?></th>
		<th class="data"><?php echo __('Actions'); ?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($famille['Plante'] as $plante): ?>
		<tr>
			<td class="data"><?php echo h ($plante['id']); ?></td>
			<td class="data"><?php echo $plante['nom']; ?></td>
			<td class="data"><?php echo $plante['definition']; ?></td>
			<td class="data"><?php echo $plante['caracteristiques']; ?></td>
			<td class="data"><?php echo $plante['image']; ?></td>
			<td class="data"><?php echo $plante['prix']; ?></td>
			<td class="data"><?php echo $plante['quantite']; ?></td>
			<td class="data"><?php echo $plante['famille_id']; ?></td>
			<td class="data"><?php echo $plante['secteur_id']; ?></td>
			<td class="data">
				<?php echo $this->Html->link(__('Afficher|'), array('controller' => 'plantes', 'action' => 'view', $plante['id'])); ?>
				<?php echo $this->Html->link(__('Modifier|'), array('controller' => 'plantes', 'action' => 'edit', $plante['id'])); ?>
				<?php echo $this->Form->postLink(__('Supprimer'), array('controller' => 'plantes', 'action' => 'delete', $plante['id']), null, __('Are you sure you want to delete # %s?', $plante['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
