<div class="familles form">
<?php echo $this->Form->create('Famille'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Famille'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('famille');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Famille.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Famille.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Familles'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
