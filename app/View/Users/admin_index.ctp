<div>
	<h3>Liste des utilisateurs </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('login'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('password'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('client_id'); ?></th>
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($users as $user):?>
	<tr>
			<td class="data"><?php echo h($user['User']['id']); ?>&nbsp;</td>
			<td class="data"><?php echo h($user['User']['login']); ?>&nbsp;</td>
		<td class="data"><?php echo h($user['User']['password']); ?>&nbsp;</td>
         <td class="data"><?php echo h($user['User']['client_id']); ?>&nbsp;</td>
             <td  class="data">
			
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
