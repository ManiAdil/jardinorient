<div class="users view">
<p class="inscriptionf"><?php  echo __('User'); ?> :</p>
	<dl>
		<u><b><dt><?php echo __('Id'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($user['User']['id']); ?><br>
			&nbsp;
		</dd>
		<b><u><dt><?php echo __('Login'); ?> :</b></u></dt><br>
		<dd>
			<?php echo h($user['User']['login']); ?><br>
			&nbsp;
		</dd>
		<b><u><dt><?php echo __('Password'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($user['User']['password']); ?><br>
			&nbsp;
		</dd>
		<b><u><dt><?php echo __('Client Id'); ?> :</dt></b></u><br>
		<dd>
			<?php echo h($user['User']['client_id']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php echo __('Related Clients'); ?></h3>
	<?php if (!empty($user['Client'])): ?>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
		<dd>
	<?php echo $user['Client']['id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Nom'); ?></dt>
		<dd>
	<?php echo $user['Client']['nom']; ?>
&nbsp;</dd>
		<dt><?php echo __('Prenom'); ?></dt>
		<dd>
	<?php echo $user['Client']['prenom']; ?>
&nbsp;</dd>
		<dt><?php echo __('Adresse'); ?></dt>
		<dd>
	<?php echo $user['Client']['adresse']; ?>
&nbsp;</dd>
		<dt><?php echo __('Tel'); ?></dt>
		<dd>
	<?php echo $user['Client']['tel']; ?>
&nbsp;</dd>
		<dt><?php echo __('Ville Id'); ?></dt>
		<dd>
	<?php echo $user['Client']['ville_id']; ?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('Edit Client'), array('controller' => 'clients', 'action' => 'edit', $user['Client']['id'])); ?></li>
			</ul>
		</div>
	</div>
	