<div class="news view">
<h2><?php  echo __('News'); ?></h2>
	<dl>
		<dt><h4><u><?php echo __('Id:'); ?></h4></u></dt>
		<dd>
			<?php echo h($news['News']['id']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Titre:'); ?></h4></u></dt>
		<dd>
			<?php echo h($news['News']['titre']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Texte:'); ?></h4></u></dt>
		<dd>
			<?php echo h($news['News']['texte']); ?>
			&nbsp;
		</dd>
	
		<dt><h4><u><?php echo __('Created:'); ?></h4></u></dt>
		<dd>
			<?php echo h($news['News']['created']); ?>
			&nbsp;
		</dd>
		<dt><h4><u><?php echo __('Updated:'); ?></h4></u></dt>
		<dd>
			<?php echo h($news['News']['updated']); ?>
			&nbsp;
		</dd>
        	<dt><h4><u><?php echo __('Image:'); ?></h4></u></dt>
		<dd>
        
        	<?php echo $this->Html->image("upload/".$news['News']['image']);?>

		
		</dd>
        
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit News'), array('action' => 'edit', $news['News']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete News'), array('action' => 'delete', $news['News']['id']), null, __('Are you sure you want to delete # %s?', $news['News']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List News'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New News'), array('action' => 'add')); ?> </li>
	</ul>
</div>
