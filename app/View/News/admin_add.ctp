<div class="news form">
<?php echo $this->Form->create('News',array('type'=>'file')); ?>
	<fieldset>
		<legend><?php echo __('Admin Add News'); ?></legend>
	<?php
		echo $this->Form->input('titre');
		echo $this->Form->input('texte');
        echo $this->Form->input('updated');
        echo $this->Form->input('created');
        echo $this->Form->input('image',array('type'=>'file','error'=>array('attributes'=>array('wrap'=>'span','class'=>'help-inline error')),'label'=>false,"class"=>"span")); 
        
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List News'), array('action' => 'index')); ?></li>
	</ul>
</div>
