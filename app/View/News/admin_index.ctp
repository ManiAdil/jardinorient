<div>
	<h3>Liste des nouveautés </h3>
	<table class="data">
	<tr class="data">
			<th class="data"><?php echo $this->Paginator->sort('id'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('titre'); ?></th>
			<th class="data"><?php echo $this->Paginator->sort('texte'); ?></th>
            <th class="data"><?php echo $this->Paginator->sort('image'); ?></th>
            <th class="data"><?php echo $this->Paginator->sort('created'); ?></th>
         <th class="data"><?php echo $this->Paginator->sort('updated'); ?></th>
			<th  class="data">Actions</th>
	</tr>
	<?php foreach ($news as $new):?>
	<tr>
			<td class="data"><?php echo h($new['News']['id']); ?>&nbsp;</td>
			<td class="data"><?php echo h($new['News']['titre']); ?>&nbsp;</td>
		<td class="data"><?php echo h($new['News']['texte']); ?>&nbsp;</td>
     	<td class="data"><?php echo h($new['News']['image']); ?>&nbsp;</td>
		<td class="data"><?php echo h($new['News']['created']); ?>&nbsp;</td>
		<td class="data"><?php echo h($new['News']['updated']); ?>&nbsp;</td>
             <td  class="data">
			<?php echo $this->Html->link(__('Afficher|'), array('action' => 'view', $new['News']['id'])); ?>
			<?php echo $this->Html->link(__('Modifier|'), array('action' => 'edit', $new['News']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $new['News']['id']), null, __('Are you sure you want to delete # %s?', $new['News']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} de {:pages}, Affichage de  {:current} enregistrements du Total : {:count} , Départ {:start}, Fin {:end}')
	));
	?>
	<?php
		echo $this->Paginator->prev('   << ' . __('Précedant'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('   -   Suivant') . ' >>', array(), null, array('class' => 'next disabled'));
	?>	</p>



</div>
