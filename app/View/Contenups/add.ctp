<div class="contenups form">
<?php echo $this->Form->create('Contenup'); ?>
	<fieldset>
		<legend><?php echo __('Add Contenup'); ?></legend>
	<?php
		echo $this->Form->input('commande_id');
		echo $this->Form->input('plante_id');
		echo $this->Form->input('quantitep');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Contenups'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
