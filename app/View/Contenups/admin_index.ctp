<div class="contenups index">
	<h2><?php echo __('Contenups'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('commande_id'); ?></th>
			<th><?php echo $this->Paginator->sort('plante_id'); ?></th>
			<th><?php echo $this->Paginator->sort('quantitep'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($contenups as $contenup): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($contenup['Commande']['id'], array('controller' => 'commandes', 'action' => 'view', $contenup['Commande']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($contenup['Plante']['id'], array('controller' => 'plantes', 'action' => 'view', $contenup['Plante']['id'])); ?>
		</td>
		<td><?php echo h($contenup['Contenup']['quantitep']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $contenup['Contenup']['commande_id plante_id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $contenup['Contenup']['commande_id plante_id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $contenup['Contenup']['commande_id plante_id']), null, __('Are you sure you want to delete # %s?', $contenup['Contenup']['commande_id plante_id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Contenup'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
