<div class="contenups view">
<h2><?php  echo __('Contenup'); ?></h2>
	<dl>
		<dt><?php echo __('Commande'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contenup['Commande']['id'], array('controller' => 'commandes', 'action' => 'view', $contenup['Commande']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plante'); ?></dt>
		<dd>
			<?php echo $this->Html->link($contenup['Plante']['id'], array('controller' => 'plantes', 'action' => 'view', $contenup['Plante']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Quantitep'); ?></dt>
		<dd>
			<?php echo h($contenup['Contenup']['quantitep']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Contenup'), array('action' => 'edit', $contenup['Contenup']['commande_id plante_id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Contenup'), array('action' => 'delete', $contenup['Contenup']['commande_id plante_id']), null, __('Are you sure you want to delete # %s?', $contenup['Contenup']['commande_id plante_id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Contenups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Contenup'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Commandes'), array('controller' => 'commandes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Commande'), array('controller' => 'commandes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
