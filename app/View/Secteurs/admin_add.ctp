<div class="secteurs form">
<?php echo $this->Form->create('Secteur'); ?>
	<fieldset>
		<legend><?php echo __('Admin Add Secteur'); ?></legend>
	<?php
		echo $this->Form->input('secteur');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Secteurs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
