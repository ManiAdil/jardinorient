<div class="secteurs form">
<?php echo $this->Form->create('Secteur'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Secteur'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('secteur');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Secteur.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Secteur.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Secteurs'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Plantes'), array('controller' => 'plantes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plante'), array('controller' => 'plantes', 'action' => 'add')); ?> </li>
	</ul>
</div>
