<?php
App::uses('AppController', 'Controller');
/**
 * Outils Controller
 *
 * @property Outil $Outil
 */
class OutilsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Outil->recursive = 0;
		$this->set('outil', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Outil->exists($id)) {
			throw new NotFoundException(__('Invalid outil'));
		}
		$options = array('conditions' => array('Outil.' . $this->Outil->primaryKey => $id));
		$this->set('outil', $this->Outil->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Outil->create();
			if ($this->Outil->save($this->request->data)) {
				$this->Session->setFlash(__('The outil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outil could not be saved. Please, try again.'));
			}
		}
		$typesoutils = $this->Outil->Typesoutil->find('list');
		$this->set(compact('typesoutils'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Outil->exists($id)) {
			throw new NotFoundException(__('Invalid outil'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Outil->save($this->request->data)) {
				$this->Session->setFlash(__('The outil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outil could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Outil.' . $this->Outil->primaryKey => $id));
			$this->request->data = $this->Outil->find('first', $options);
		}
		$typesoutils = $this->Outil->Typesoutil->find('list');
		$this->set(compact('typesoutils'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Outil->id = $id;
		if (!$this->Outil->exists()) {
			throw new NotFoundException(__('Invalid outil'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Outil->delete()) {
			$this->Session->setFlash(__('Outil deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Outil was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	$this->layout="admin";
		$this->Outil->recursive = 0;
		$this->set('outils', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Outil->exists($id)) {
			throw new NotFoundException(__('Invalid outil'));
		}
		$options = array('conditions' => array('Outil.' . $this->Outil->primaryKey => $id));
		$this->set('outil', $this->Outil->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
	
		if ($this->request->is('post')) {
		$this->Upload->upload('outil','image','image');
			$this->Outil->create();
			if ($this->Outil->save($this->request->data)) {
				$this->Session->setFlash(__('The outil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outil could not be saved. Please, try again.'));
			}
		}
		$typesoutils = $this->Outil->Typesoutil->find('list');
		$this->set(compact('typesoutils'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Outil->exists($id)) {
			throw new NotFoundException(__('Invalid outil'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
		$this->Upload->upload('News','image','image');
			if ($this->Outil->save($this->request->data)) {
				$this->Session->setFlash(__('The outil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The outil could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Outil.' . $this->Outil->primaryKey => $id));
			$this->request->data = $this->Outil->find('first', $options);
		}
		$typesoutils = $this->Outil->Typesoutil->find('list');
		$this->set(compact('typesoutils'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Outil->id = $id;
		if (!$this->Outil->exists()) {
			throw new NotFoundException(__('Invalid outil'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Outil->delete()) {
			$this->Session->setFlash(__('Outil deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Outil was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
