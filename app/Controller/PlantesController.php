<?php
App::uses('AppController', 'Controller');
/**
 * Plantes Controller
 *
 * @property Plante $Plante
 */
class PlantesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Plante->recursive = 0;
	
	

          	
        $this->set('plantes', $this->Plante->findAllByfamille_id($this->request->params['famille']));
      
	}
    	public function index_secteur() {
		$this->Plante->recursive = 0;
	
	$this->Plante->recursive = 0;
          $this->set('plantes', $this->Plante->findAllBysecteur_id($this->request->params['secteur']));
          	
   
      
	}
	public function total() {
		$this->Plante->recursive = 0;
		$this->set('plantes', $this->paginate());
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Plante->recursive = 0;
		$this->set('plantes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Plante->exists($id)) {
			throw new NotFoundException(__('Invalid plante'));
		}
		$options = array('conditions' => array('Plante.' . $this->Plante->primaryKey => $id));
		$this->set('plante', $this->Plante->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
     $this->Upload->upload('Plante','image','image');
		if ($this->request->is('post')) { 
			$this->Plante->create();
			if ($this->Plante->save($this->request->data)) {
				$this->Session->setFlash(__('The plante has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plante could not be saved. Please, try again.'));
			}
		}
		$familles = $this->Plante->Famille->find('list');
		$secteurs = $this->Plante->Secteur->find('list');
		$this->set(compact('familles', 'secteurs'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
     $this->Upload->upload('Plante','image','image');
		if (!$this->Plante->exists($id)) {
			throw new NotFoundException(__('Invalid plante'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Plante->save($this->request->data)) {
				$this->Session->setFlash(__('The plante has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The plante could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plante.' . $this->Plante->primaryKey => $id));
			$this->request->data = $this->Plante->find('first', $options);
		}
		$familles = $this->Plante->Famille->find('list');
		$secteurs = $this->Plante->Secteur->find('list');
		$this->set(compact('familles', 'secteurs'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Plante->id = $id;
		if (!$this->Plante->exists()) {
			throw new NotFoundException(__('Invalid plante'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Plante->delete()) {
			$this->Session->setFlash(__('Plante deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Plante was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
