<?php
App::uses('AppController', 'Controller');
/**
 * Commandes Controller
 *
 * @property Commande $Commande
 */
class CommandesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Commande->recursive = 0;
		$this->set('commandes', $this->paginate());
	}



/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Commande->create();
			if ($this->Commande->save($this->request->data)) {
				$this->Session->setFlash(__('The commande has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commande could not be saved. Please, try again.'));
			}
		}
		$clients = $this->Commande->Client->find('list');
		$this->set(compact('clients'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Commande->id = $id;
		if (!$this->Commande->exists()) {
			throw new NotFoundException(__('Invalid commande'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Commande->delete()) {
			$this->Session->setFlash(__('Commande deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Commande was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Commande->recursive = 0;
		$this->set('commandes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Commande->exists($id)) {
			throw new NotFoundException(__('Invalid commande'));
		}
		$options = array('conditions' => array('Commande.' . $this->Commande->primaryKey => $id));
		$this->set('commande', $this->Commande->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
		if ($this->request->is('post')) {
			$this->Commande->create();
			if ($this->Commande->save($this->request->data)) {
				$this->Session->setFlash(__('The commande has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commande could not be saved. Please, try again.'));
			}
		}
		$clients = $this->Commande->Client->find('list');
		$this->set(compact('clients'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Commande->exists($id)) {
			throw new NotFoundException(__('Invalid commande'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Commande->save($this->request->data)) {
				$this->Session->setFlash(__('The commande has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The commande could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Commande.' . $this->Commande->primaryKey => $id));
			$this->request->data = $this->Commande->find('first', $options);
		}
		$clients = $this->Commande->Client->find('list');
		$this->set(compact('clients'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Commande->id = $id;
		if (!$this->Commande->exists()) {
			throw new NotFoundException(__('Invalid commande'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Commande->delete()) {
			$this->Session->setFlash(__('Commande deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Commande was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
