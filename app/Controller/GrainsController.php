<?php
App::uses('AppController', 'Controller');
/**
 * Grains Controller
 *
 * @property Grain $Grain
 */
class GrainsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Grain->recursive = 0;
		$this->set('grains', $this->paginate());
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
	  // Cache::clear();
		$this->Grain->recursive = 0;
		$this->set('grains', $this->paginate());
		$this->layout = "admin";
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
	$this->layout = "admin";
		if (!$this->Grain->exists($id)) {
			throw new NotFoundException(__('Invalid grain'));
		}
		$options = array('conditions' => array('Grain.' . $this->Grain->primaryKey => $id));
		$this->set('grain', $this->Grain->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout = "admin";
    
		if ($this->request->is('post')) {
		$this->Upload->upload('Grain','image','image');
			$this->Grain->create();
			if ($this->Grain->save($this->request->data)) {
				$this->Session->setFlash(__('The grain has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grain could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout = "admin";
	$this->Upload->upload('Grains','image','image');
		if (!$this->Grain->exists($id)) {
			throw new NotFoundException(__('Invalid grain'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Grain->save($this->request->data)) {
				$this->Session->setFlash(__('The grain has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The grain could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Grain.' . $this->Grain->primaryKey => $id));
			$this->request->data = $this->Grain->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout = "admin";
		$this->Grain->id = $id;
		if (!$this->Grain->exists()) {
			throw new NotFoundException(__('Invalid grain'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Grain->delete()) {
			$this->Session->setFlash(__('Grain deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Grain was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
