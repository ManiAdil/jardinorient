<?php
App::uses('AppController', 'Controller');
/**
 * Factures Controller
 *
 * @property Facture $Facture
 */
class FacturesController extends AppController {


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Facture->exists($id)) {
			throw new NotFoundException(__('Invalid facture'));
		}
		$options = array('conditions' => array('Facture.' . $this->Facture->primaryKey => $id));
		$this->set('facture', $this->Facture->find('first', $options));
	}


/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Facture->recursive = 0;
		$this->set('factures', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Facture->exists($id)) {
			throw new NotFoundException(__('Invalid facture'));
		}
		$options = array('conditions' => array('Facture.' . $this->Facture->primaryKey => $id));
		$this->set('facture', $this->Facture->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
		if ($this->request->is('post')) {
			$this->Facture->create();
			if ($this->Facture->save($this->request->data)) {
				$this->Session->setFlash(__('The facture has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The facture could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Facture->exists($id)) {
			throw new NotFoundException(__('Invalid facture'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Facture->save($this->request->data)) {
				$this->Session->setFlash(__('The facture has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The facture could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Facture.' . $this->Facture->primaryKey => $id));
			$this->request->data = $this->Facture->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Facture->id = $id;
		if (!$this->Facture->exists()) {
			throw new NotFoundException(__('Invalid facture'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Facture->delete()) {
			$this->Session->setFlash(__('Facture deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Facture was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
