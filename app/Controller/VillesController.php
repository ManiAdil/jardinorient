<?php
App::uses('AppController', 'Controller');
/**
 * Villes Controller
 *
 * @property Ville $Ville
 */
class VillesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Ville->recursive = 0;
		$this->set('villes', $this->paginate());
	}

/**

 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ville->exists($id)) {
			throw new NotFoundException(__('Invalid ville'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}



	

	public function admin_index() {$this->layout="admin";


		$this->Ville->recursive = 0;
		$this->set('villes', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */



	public function admin_view($id = null) {$this->layout = "admin";

		if (!$this->Ville->exists($id)) {
			throw new NotFoundException(__('Invalid ville'));
		}
		$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
		$this->set('ville', $this->Ville->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */

	

	public function admin_add() {$this->layout = "admin";

		if ($this->request->is('post')) {
			$this->Ville->create();
			if ($this->Ville->save($this->request->data)) {
				$this->Session->setFlash(__('The ville has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ville could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	

	public function admin_edit($id = null) {$this->layout = "admin";

		if (!$this->Ville->exists($id)) {
			throw new NotFoundException(__('Invalid ville'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Ville->save($this->request->data)) {
				$this->Session->setFlash(__('The ville has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ville could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ville.' . $this->Ville->primaryKey => $id));
			$this->request->data = $this->Ville->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */

	

	public function admin_delete($id = null) {$this->layout = "admin";

		$this->Ville->id = $id;
		if (!$this->Ville->exists()) {
			throw new NotFoundException(__('Invalid ville'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Ville->delete()) {
			$this->Session->setFlash(__('Ville deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Ville was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
