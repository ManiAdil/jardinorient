<?php
App::uses('AppController', 'Controller');
/**
 * Directeurs Controller
 *
 * @property Directeur $Directeur
 */
class DirecteursController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Directeur->recursive = 0;
		$this->set('directeurs', $this->paginate());
        
       
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
	$this->layout = "admin";
		if (!$this->Directeur->exists($id)) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		$options = array('conditions' => array('Directeur.' . $this->Directeur->primaryKey => $id));
		$this->set('directeur', $this->Directeur->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
	$this->layout = "admin";
		if ($this->request->is('post')) {
			$this->Directeur->create();
			if ($this->Directeur->save($this->request->data)) {
				$this->Session->setFlash(__('The directeur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The directeur could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
	$this->layout = "admin";
		if (!$this->Directeur->exists($id)) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Directeur->save($this->request->data)) {
				$this->Session->setFlash(__('The directeur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The directeur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Directeur.' . $this->Directeur->primaryKey => $id));
			$this->request->data = $this->Directeur->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
	$this->layout = "admin";
		$this->Directeur->id = $id;
		if (!$this->Directeur->exists()) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Directeur->delete()) {
			$this->Session->setFlash(__('Directeur deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Directeur was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Directeur->recursive = 0;
		$this->set('directeurs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Directeur->exists($id)) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		$options = array('conditions' => array('Directeur.' . $this->Directeur->primaryKey => $id));
		$this->set('directeur', $this->Directeur->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Directeur->create();
			if ($this->Directeur->save($this->request->data)) {
				$this->Session->setFlash(__('The directeur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The directeur could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
	   $this->layout = "admin";
		if (!$this->Directeur->exists($id)) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Directeur->save($this->request->data)) {
				$this->Session->setFlash(__('The directeur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The directeur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Directeur.' . $this->Directeur->primaryKey => $id));
			$this->request->data = $this->Directeur->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Directeur->id = $id;
		if (!$this->Directeur->exists()) {
			throw new NotFoundException(__('Invalid directeur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Directeur->delete()) {
			$this->Session->setFlash(__('Directeur deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Directeur was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
