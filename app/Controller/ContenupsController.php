<?php
App::uses('AppController', 'Controller');
/**
 * Contenups Controller
 *
 * @property Contenup $Contenup
 */
class ContenupsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contenup->recursive = 0;
		$this->set('contenups', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Contenup->exists($id)) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		$options = array('conditions' => array('Contenup.' . $this->Contenup->primaryKey => $id));
		$this->set('contenup', $this->Contenup->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contenup->create();
			if ($this->Contenup->save($this->request->data)) {
				$this->Session->setFlash(__('The contenup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenup could not be saved. Please, try again.'));
			}
		}
		$commandes = $this->Contenup->Commande->find('list');
		$plantes = $this->Contenup->Plante->find('list');
		$this->set(compact('commandes', 'plantes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Contenup->exists($id)) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contenup->save($this->request->data)) {
				$this->Session->setFlash(__('The contenup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenup could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contenup.' . $this->Contenup->primaryKey => $id));
			$this->request->data = $this->Contenup->find('first', $options);
		}
		$commandes = $this->Contenup->Commande->find('list');
		$plantes = $this->Contenup->Plante->find('list');
		$this->set(compact('commandes', 'plantes'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Contenup->id = $id;
		if (!$this->Contenup->exists()) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contenup->delete()) {
			$this->Session->setFlash(__('Contenup deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contenup was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Contenup->recursive = 0;
		$this->set('contenups', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Contenup->exists($id)) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		$options = array('conditions' => array('Contenup.' . $this->Contenup->primaryKey => $id));
		$this->set('contenup', $this->Contenup->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Contenup->create();
			if ($this->Contenup->save($this->request->data)) {
				$this->Session->setFlash(__('The contenup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenup could not be saved. Please, try again.'));
			}
		}
		$commandes = $this->Contenup->Commande->find('list');
		$plantes = $this->Contenup->Plante->find('list');
		$this->set(compact('commandes', 'plantes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Contenup->exists($id)) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contenup->save($this->request->data)) {
				$this->Session->setFlash(__('The contenup has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenup could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contenup.' . $this->Contenup->primaryKey => $id));
			$this->request->data = $this->Contenup->find('first', $options);
		}
		$commandes = $this->Contenup->Commande->find('list');
		$plantes = $this->Contenup->Plante->find('list');
		$this->set(compact('commandes', 'plantes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Contenup->id = $id;
		if (!$this->Contenup->exists()) {
			throw new NotFoundException(__('Invalid contenup'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contenup->delete()) {
			$this->Session->setFlash(__('Contenup deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contenup was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
