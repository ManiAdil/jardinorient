<?php
App::uses('AppController', 'Controller');
/**
 * Secteurs Controller
 *
 * @property Secteur $Secteur
 */
class SecteursController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Secteur->recursive = 0;
		$this->set('secteurs', $this->paginate());
	}

/**
 
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {	$this->layout="admin";

		$this->Secteur->recursive = 0;
		$this->set('secteurs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {	$this->layout="admin";

		if (!$this->Secteur->exists($id)) {
			throw new NotFoundException(__('Invalid secteur'));
		}
		$options = array('conditions' => array('Secteur.' . $this->Secteur->primaryKey => $id));
		$this->set('secteur', $this->Secteur->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {	$this->layout="admin";

		if ($this->request->is('post')) {
			$this->Secteur->create();
			if ($this->Secteur->save($this->request->data)) {
				$this->Session->setFlash(__('The secteur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The secteur could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {	$this->layout="admin";

		if (!$this->Secteur->exists($id)) {
			throw new NotFoundException(__('Invalid secteur'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Secteur->save($this->request->data)) {
				$this->Session->setFlash(__('The secteur has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The secteur could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Secteur.' . $this->Secteur->primaryKey => $id));
			$this->request->data = $this->Secteur->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {	$this->layout="admin";

		$this->Secteur->id = $id;
		if (!$this->Secteur->exists()) {
			throw new NotFoundException(__('Invalid secteur'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Secteur->delete()) {
			$this->Session->setFlash(__('Secteur deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Secteur was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
