<?php
App::uses('AppController', 'Controller');
/**
 * Livraisons Controller
 *
 * @property Livraison $Livraison
 */
class LivraisonsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Livraison->recursive = 0;
		$this->set('livraisons', $this->paginate());
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {$this->layout="admin";
		$this->Livraison->recursive = 0;
		$this->set('livraisons', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout="admin";
		if (!$this->Livraison->exists($id)) {
			throw new NotFoundException(__('Invalid livraison'));
		}
		$options = array('conditions' => array('Livraison.' . $this->Livraison->primaryKey => $id));
		$this->set('livraison', $this->Livraison->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout="admin";
		if ($this->request->is('post')) {
			$this->Livraison->create();
			if ($this->Livraison->save($this->request->data)) {
				$this->Session->setFlash(__('The livraison has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The livraison could not be saved. Please, try again.'));
			}
		}
		$factures = $this->Livraison->facture->find('list');
		$commandes = $this->Livraison->Commande->find('list');
		$this->set(compact('factures', 'commandes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout="admin";
		if (!$this->Livraison->exists($id)) {
			throw new NotFoundException(__('Invalid livraison'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Livraison->save($this->request->data)) {
				$this->Session->setFlash(__('The livraison has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The livraison could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Livraison.' . $this->Livraison->primaryKey => $id));
			$this->request->data = $this->Livraison->find('first', $options);
		}
		$factures = $this->Livraison->Facture->find('list');
		$commandes = $this->Livraison->Commande->find('list');
		$this->set(compact('factures', 'commandes'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout="admin";
		$this->Livraison->id = $id;
		if (!$this->Livraison->exists()) {
			throw new NotFoundException(__('Invalid livraison'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Livraison->delete()) {
			$this->Session->setFlash(__('Livraison deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Livraison was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
