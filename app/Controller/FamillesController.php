<?php
App::uses('AppController', 'Controller');
/**
 * Familles Controller
 *
 * @property Famille $Famille
 */
class FamillesController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Famille->recursive = 0;
		$this->set('familles', $this->paginate());
	}



/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {	$this->layout="admin";

		$this->Famille->recursive = 0;
		$this->set('familles', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) { 	$this->layout="admin";

		if (!$this->Famille->exists($id)) {
			throw new NotFoundException(__('Invalid famille'));
		}
		$options = array('conditions' => array('Famille.' . $this->Famille->primaryKey => $id));
		$this->set('famille', $this->Famille->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {	$this->layout="admin";

		if ($this->request->is('post')) {
			$this->Famille->create();
			if ($this->Famille->save($this->request->data)) {
				$this->Session->setFlash(__('The famille has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The famille could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {	$this->layout="admin";

		if (!$this->Famille->exists($id)) {
			throw new NotFoundException(__('Invalid famille'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Famille->save($this->request->data)) {
				$this->Session->setFlash(__('The famille has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The famille could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Famille.' . $this->Famille->primaryKey => $id));
			$this->request->data = $this->Famille->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {	$this->layout="admin";

		$this->Famille->id = $id;
		if (!$this->Famille->exists()) {
			throw new NotFoundException(__('Invalid famille'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Famille->delete()) {
			$this->Session->setFlash(__('Famille deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Famille was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
