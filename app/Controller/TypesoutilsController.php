<?php
App::uses('AppController', 'Controller');
/**
 * Typesoutils Controller
 *
 * @property Typesoutil $Typesoutil
 */
class TypesoutilsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Typesoutil->recursive = 0;
		$this->set('typesoutils', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		if (!$this->Typesoutil->exists($id)) {
			throw new NotFoundException(__('Invalid typesoutil'));
		}
		$options = array('conditions' => array('Typesoutil.' . $this->Typesoutil->primaryKey => $id));
		$this->set('typesoutil', $this->Typesoutil->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = "admin";
		$this->Typesoutil->recursive = 0;
		$this->set('typesoutils', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {$this->layout = "admin";
		if (!$this->Typesoutil->exists($id)) {
			throw new NotFoundException(__('Invalid typesoutil'));
		}
		$options = array('conditions' => array('Typesoutil.' . $this->Typesoutil->primaryKey => $id));
		$this->set('typesoutil', $this->Typesoutil->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {$this->layout = "admin";
		if ($this->request->is('post')) {
			$this->Typesoutil->create();
			if ($this->Typesoutil->save($this->request->data)) {
				$this->Session->setFlash(__('The typesoutil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The typesoutil could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {$this->layout = "admin";
		if (!$this->Typesoutil->exists($id)) {
			throw new NotFoundException(__('Invalid typesoutil'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Typesoutil->save($this->request->data)) {
				$this->Session->setFlash(__('The typesoutil has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The typesoutil could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Typesoutil.' . $this->Typesoutil->primaryKey => $id));
			$this->request->data = $this->Typesoutil->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {$this->layout = "admin";
		$this->Typesoutil->id = $id;
		if (!$this->Typesoutil->exists()) {
			throw new NotFoundException(__('Invalid typesoutil'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Typesoutil->delete()) {
			$this->Session->setFlash(__('Typesoutil deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Typesoutil was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
