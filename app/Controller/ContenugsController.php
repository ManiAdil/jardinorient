<?php
App::uses('AppController', 'Controller');
/**
 * Contenugs Controller
 *
 * @property Contenug $Contenug
 */
class ContenugsController extends AppController {

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Contenug->recursive = 0;
		$this->set('contenugs', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Contenug->exists($id)) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		$options = array('conditions' => array('Contenug.' . $this->Contenug->primaryKey => $id));
		$this->set('contenug', $this->Contenug->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Contenug->create();
			if ($this->Contenug->save($this->request->data)) {
				$this->Session->setFlash(__('The contenug has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenug could not be saved. Please, try again.'));
			}
		}
		$commandes = $this->Contenug->Commande->find('list');
		$grains = $this->Contenug->Grain->find('list');
		$this->set(compact('commandes', 'grains'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Contenug->exists($id)) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contenug->save($this->request->data)) {
				$this->Session->setFlash(__('The contenug has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenug could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contenug.' . $this->Contenug->primaryKey => $id));
			$this->request->data = $this->Contenug->find('first', $options);
		}
		$commandes = $this->Contenug->Commande->find('list');
		$grains = $this->Contenug->Grain->find('list');
		$this->set(compact('commandes', 'grains'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Contenug->id = $id;
		if (!$this->Contenug->exists()) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contenug->delete()) {
			$this->Session->setFlash(__('Contenug deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contenug was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Contenug->recursive = 0;
		$this->set('contenugs', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Contenug->exists($id)) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		$options = array('conditions' => array('Contenug.' . $this->Contenug->primaryKey => $id));
		$this->set('contenug', $this->Contenug->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Contenug->create();
			if ($this->Contenug->save($this->request->data)) {
				$this->Session->setFlash(__('The contenug has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenug could not be saved. Please, try again.'));
			}
		}
		$commandes = $this->Contenug->Commande->find('list');
		$grains = $this->Contenug->Grain->find('list');
		$this->set(compact('commandes', 'grains'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Contenug->exists($id)) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Contenug->save($this->request->data)) {
				$this->Session->setFlash(__('The contenug has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The contenug could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Contenug.' . $this->Contenug->primaryKey => $id));
			$this->request->data = $this->Contenug->find('first', $options);
		}
		$commandes = $this->Contenug->Commande->find('list');
		$grains = $this->Contenug->Grain->find('list');
		$this->set(compact('commandes', 'grains'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Contenug->id = $id;
		if (!$this->Contenug->exists()) {
			throw new NotFoundException(__('Invalid contenug'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Contenug->delete()) {
			$this->Session->setFlash(__('Contenug deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Contenug was not deleted'));
		$this->redirect(array('action' => 'index'));
	}
}
