<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    private $widget= array();
     public $components = array('Upload','Auth','Session');
    function beforeFilter()
    {
        cache::clear();
        $this->configAuth();
        if($this->request->prefix == "admin")
        {
            
            
        }else
        {
            $this->Auth->allow();
            
        }
         $this->searchNews();
       $this->searchMotDirecteur();
        $this->searchSitesAmi();
       $this->searchcoordonees();
       $this->searchfamille();
             
        $this->set("widget",$this->widget);     
    }
    private function configAuth()
    {
          
      $this->Auth->authenticate = array(
            'all' => array('userModel' => 'User',
                           'fields'=>array('password'=>'password','username'=>'login'),
                           ),
            'Form',
        );
    
        $this->Auth->loginAction = '/login.html';
        $this->Auth->logoutAction= '/logout.html';
        $this->Auth->loginRedirect='/admin';
        $this->Auth->loginError = 'Votre mot de passe ou bien votre login ne sont pas corréctes !!';
        
    }
    

    
    private function searchSitesAmi(){
      $this->uses[]= "SitesAmi";
      $this->SitesAmi->recursive = 0;
      $this->widget["SitesAmis"]= $this->SitesAmi->find('all',array('fields'=>array('titre',"url")));
    }

    private function searchNews(){
      $this->uses[]= "News";
      $this->News->recursive = 0;
      $this->widget["News"]= $this->News->find('all',array('order'=>array('News.created'=>"DESC"),'limit'=>5));
    }

    private function searchMotDirecteur(){
      $this->uses[]= "Directeur";
      $this->Directeur->recursive = 0;
      $this->widget["Directeur"]= $this->Directeur->find('first');
    }
    private function searchcoordonees(){
      $this->uses[]= "Information";
      $this->Information->recursive = 0;
      $this->widget["Informations"]= $this->Information->find('first');    }

    private function searchfamille(){
      $this->uses[]= "Famille";
      $this->Famille->recursive = 0;
      $this->widget["Familles"]= $this->Famille->find('all',array('fields'=>array('id','famille')));    }

}