<?php
App::uses('Directeur', 'Model');

/**
 * Directeur Test Case
 *
 */
class DirecteurTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.directeur'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Directeur = ClassRegistry::init('Directeur');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Directeur);

		parent::tearDown();
	}

}
