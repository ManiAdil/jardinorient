<?php
App::uses('Secteur', 'Model');

/**
 * Secteur Test Case
 *
 */
class SecteurTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.secteur',
		'app.plante',
		'app.famille'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Secteur = ClassRegistry::init('Secteur');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Secteur);

		parent::tearDown();
	}

}
