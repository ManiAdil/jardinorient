<?php
App::uses('Grain', 'Model');

/**
 * Grain Test Case
 *
 */
class GrainTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.grain'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Grain = ClassRegistry::init('Grain');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Grain);

		parent::tearDown();
	}

}
