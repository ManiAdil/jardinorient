<?php
App::uses('Outil', 'Model');

/**
 * Outil Test Case
 *
 */
class OutilTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.outil',
		'app.typesoutil'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Outil = ClassRegistry::init('Outil');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Outil);

		parent::tearDown();
	}

}
