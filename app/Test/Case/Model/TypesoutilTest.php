<?php
App::uses('Typesoutil', 'Model');

/**
 * Typesoutil Test Case
 *
 */
class TypesoutilTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.typesoutil',
		'app.outil'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Typesoutil = ClassRegistry::init('Typesoutil');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Typesoutil);

		parent::tearDown();
	}

}
