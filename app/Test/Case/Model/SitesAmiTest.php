<?php
App::uses('SitesAmi', 'Model');

/**
 * SitesAmi Test Case
 *
 */
class SitesAmiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.sites_ami'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->SitesAmi = ClassRegistry::init('SitesAmi');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->SitesAmi);

		parent::tearDown();
	}

}
