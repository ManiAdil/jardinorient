<?php
App::uses('Plante', 'Model');

/**
 * Plante Test Case
 *
 */
class PlanteTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.plante',
		'app.famille',
		'app.secteur'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Plante = ClassRegistry::init('Plante');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Plante);

		parent::tearDown();
	}

}
