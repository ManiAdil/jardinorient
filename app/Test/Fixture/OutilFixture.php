<?php
/**
 * OutilFixture
 *
 */
class OutilFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id_o' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'nom_o' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'definition_o' => array('type' => 'text', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'photo_o' => array('type' => 'string', 'null' => false, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'prix_o' => array('type' => 'integer', 'null' => false, 'default' => null),
		'quantité_o' => array('type' => 'integer', 'null' => false, 'default' => null),
		'typesoutil_id' => array('type' => 'integer', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id_o', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id_o' => 1,
			'nom_o' => 'Lorem ipsum dolor sit amet',
			'definition_o' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'photo_o' => 'Lorem ipsum dolor sit amet',
			'prix_o' => 1,
			'quantité_o' => 1,
			'typesoutil_id' => 1
		),
	);

}
