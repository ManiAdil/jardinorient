<?php
App::uses('AppModel', 'Model');
/**
 * Secteur Model
 *
 * @property Plante $Plante
 */
class Secteur extends AppModel { public $displayField = 'secteur';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'secteur' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Plante' => array(
			'className' => 'Plante',
			'foreignKey' => 'secteur_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
