<?php
App::uses('AppModel', 'Model');
/**
 * Contenug Model
 *
 * @property Commande $Commande
 * @property Grain $Grain
 */
class Contenug extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'commande_id grain_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'quantite' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Commande' => array(
			'className' => 'Commande',
			'foreignKey' => 'commande_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Grain' => array(
			'className' => 'Grain',
			'foreignKey' => 'grain_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
