<?php
App::uses('AppModel', 'Model');
/**
 * Sitesami Model
 *
 */
class Sitesami extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'titre' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'message' => 'Le nom de site ami ne peux pas être vide',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'url' => array(
			'url' => array(
				'rule' => array('url'),
				'message' => 'Vous devez tapez un URL sous forme de Http://google.com',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
