<?php
App::uses('AppModel', 'Model');
/**
 * Famille Model
 *
 * @property Plante $Plante
 */
class Famille extends AppModel { public $displayField = 'famille';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'famille' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'Veuillez saisir un libelle de famille',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Plante' => array(
			'className' => 'Plante',
			'foreignKey' => 'famille_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
