<?php
App::uses('AppModel', 'Model');
/**
 * Contenup Model
 *
 * @property Commande $Commande
 * @property Plante $Plante
 */
class Contenup extends AppModel {

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'commande_id plante_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'quantitep' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Commande' => array(
			'className' => 'Commande',
			'foreignKey' => 'commande_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Plante' => array(
			'className' => 'Plante',
			'foreignKey' => 'plante_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
