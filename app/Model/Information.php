<?php
App::uses('AppModel', 'Model');
/**
 * Information Model
 *
 */
class Information extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'informations';

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

}
